#pragma once
#include "Actor.h"
class Turret; 
class Tank :
	public Actor
{
public:
	Tank(Game* game);
	~Tank();
	void UpdateActor(float deltaTime) override; 
	void SetPlayerTwo(); 
	void Fire(); 
	void Respawn(); 
	void SetInitialPos(Vector3 pos) 
	{ 
		mInitialPos = pos; 
		SetPosition(pos);
	}

	void ActorInput(const Uint8* keyState) override; 

protected: 
	Turret * mTurret; 
	Vector3 mInitialPos; 
	unsigned char fireKey; 
	bool mFireKeyPressed = false; 
};

