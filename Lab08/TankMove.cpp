#include "TankMove.h"
#include "SDL/SDL.h"
#include "Math.h"
#include "Block.h"
#include "Game.h"
#include <vector>
#include <algorithm>


TankMove::TankMove(Actor* actor) 
	: MoveComponent(actor)
{
	mForwardKey = SDL_SCANCODE_W;
	mBackKey = SDL_SCANCODE_S;
	mClockwiseKey = SDL_SCANCODE_D;
	mCounterClockwiseKey = SDL_SCANCODE_A;
}

void TankMove::Update(float deltaTime) 
{
	MoveComponent::Update(deltaTime);
	handleCollision();
}

void TankMove::ProcessInput(const Uint8 * keyState)
{
	SetForwardSpeed(0);
	SetAngularSpeed(0);
	if (keyState[mForwardKey])
		SetForwardSpeed(250);
	else if (keyState[mBackKey])
		SetForwardSpeed(-250);
	
	if (keyState[mCounterClockwiseKey])
		SetAngularSpeed(Math::TwoPi);
	else if (keyState[mClockwiseKey])
		SetAngularSpeed(-Math::TwoPi);
}


TankMove::~TankMove()
{
	
}

void TankMove::SetPlayerTwo()
{
	mForwardKey = SDL_SCANCODE_O;
	mBackKey = SDL_SCANCODE_L;
	mClockwiseKey = SDL_SCANCODE_SEMICOLON;
	mCounterClockwiseKey = SDL_SCANCODE_K;
}

void TankMove::handleCollision() {
	std::vector<Block*> blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {

			Vector3 blockMax = block->GetCollisionComponent()->GetMax();
			Vector3 blockMin = block->GetCollisionComponent()->GetMin();
			Vector3 playerMax = mOwner->GetCollisionComponent()->GetMax();
			Vector3 playerMin = mOwner->GetCollisionComponent()->GetMin();

			float dUp = Math::Abs(playerMax.y - blockMin.y);
			float dDown = Math::Abs(playerMin.y - blockMax.y);
			float dRight = Math::Abs(playerMin.x - blockMax.x);
			float dLeft = Math::Abs(playerMax.x - blockMin.x);

			float min = std::min(dUp, std::min(dDown, std::min(dRight, dLeft)));

			Vector3 position = mOwner->GetPosition();

			//right
			if (min == dRight) {
				mOwner->SetPosition(position + Vector3(dRight, 0, 0));
			}

			//left
			else if (min == dLeft) {
				mOwner->SetPosition(position + Vector3(-dLeft, 0, 0));
			}

			//down
			else if (min == dDown) {
				mOwner->SetPosition(position + Vector3(0, dDown, 0));
			}

			//up
			else {
				mOwner->SetPosition(position + Vector3(0, -dUp, 0));
			}

		}
	}
}
