#pragma once
#include "Actor.h"
class Tank; 
class Bullet :
	public Actor
{
public:
	Bullet(Game* game);
	~Bullet();
	void SetTank(Tank* tank) { mTank = tank; }
	void UpdateActor(float deltaTime) override;
	void handleCollision();
protected: 
	Tank * mTank; 
};

