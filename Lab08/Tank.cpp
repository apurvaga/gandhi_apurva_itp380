#include "Tank.h"
#include "Mesh.h"
#include "Game.h"
#include "Renderer.h"
#include "Turret.h"
#include "TankMove.h"
#include "Bullet.h"
#include "SDL/SDL.h"
#include <iostream>

Tank::Tank(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this); 
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/TankBase.gpmesh"));
	mMove = new TankMove(this);
	mTurret = new Turret(game);
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(30.0f, 30.0f, 30.0f); 
	fireKey = SDL_SCANCODE_LSHIFT; 
}

Tank::~Tank()
{
}

void Tank::UpdateActor(float deltaTime)
{
	mTurret->SetPosition(mPosition); 
}

void Tank::SetPlayerTwo()
{
	((TankMove*) mMove)->SetPlayerTwo(); 
	mTurret->SetPlayerTwo(); 
	mMesh->SetTextureIndex(1); 
	fireKey = SDL_SCANCODE_RSHIFT; 

}

void Tank::Fire()
{
	Bullet* bullet = new Bullet(mGame);
	bullet->SetTank(this);
	bullet->SetPosition(GetPosition());
	bullet->SetRotation(mTurret->GetRotation());
}

void Tank::Respawn()
{
	SetPosition(mInitialPos); 
	SetRotation(0.0f);
	mTurret->SetRotation(0.0f);
}

void Tank::ActorInput(const Uint8 * keyState)
{
	if (!mFireKeyPressed && keyState[fireKey]) {
		Fire(); 
	}
	mFireKeyPressed = keyState[fireKey];
}
