#pragma once
#include "MoveComponent.h"
class TankMove :
	public MoveComponent
{
public:
	TankMove(Actor* actor);
	void Update(float deltaTime) override; 
	void ProcessInput(const Uint8* keyState) override; 
	~TankMove();
	void SetPlayerTwo();
	void handleCollision();
protected:
	unsigned char mForwardKey;
	unsigned char mBackKey;
	unsigned char mClockwiseKey;
	unsigned char mCounterClockwiseKey;
};

