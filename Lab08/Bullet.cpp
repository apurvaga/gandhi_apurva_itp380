#include "Bullet.h"
#include "Game.h"
#include "Mesh.h"
#include "Renderer.h"
#include <algorithm>
#include <vector>
#include "Block.h"
#include "Game.h"
#include "Tank.h"



Bullet::Bullet(Game* game)
	: Actor(game)
{
	mMove = new MoveComponent(this);
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Sphere.gpmesh")); 
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(10.0f, 10.0f, 10.0f);
	mScale = 0.5f; 
	mMove->SetForwardSpeed(400.0f); 
}


Bullet::~Bullet()
{
}

void Bullet::UpdateActor(float deltaTime)
{
	handleCollision(); 
}

void Bullet::handleCollision() {
	std::vector<Block*> blocks = mGame->GetBlocks();
	for (auto block : blocks) {
		if (mCollide->Intersect(block->GetCollisionComponent())) {
			this->SetState(Actor::EDead); 
			return; 
		}

		else if (mCollide->Intersect(mGame->GetPlayer1()->GetCollisionComponent())) {
			if (mTank != mGame->GetPlayer1()) {
				mGame->GetPlayer1()->Respawn(); 
				this->SetState(Actor::EDead);
				return; 
			}
		}

		else if (mCollide->Intersect(mGame->GetPlayer2()->GetCollisionComponent())) {
			if (mTank != mGame->GetPlayer2()) {
				mGame->GetPlayer2()->Respawn();
				this->SetState(Actor::EDead);
				return; 
			}
		}
	}
}
