#pragma once
#include "Actor.h"
class Bullet :
	public Actor
{
public:
	Bullet(Game* game);
	~Bullet();
	void UpdateActor(float deltaTime) override; 
private: 
	float lifeTimer = 2.0f; 
};

