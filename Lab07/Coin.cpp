#include "Coin.h"
#include "Game.h"
#include "Player.h"
#include "SDL/SDL_mixer.h"
#include "AnimatedSprite.h"
#include "Game.h"

Coin::Coin(Game* game)
	:Actor(game)
{
	AnimatedSprite* sprite = new AnimatedSprite(this, 200);
	sprite->AddImages(game->getTexture("Assets/Coin/coin1.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin2.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin3.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin4.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin5.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin6.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin7.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin8.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin9.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin10.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin11.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin12.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin13.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin14.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin15.png"));
	sprite->AddImages(game->getTexture("Assets/Coin/coin16.png"));
	mSprite = sprite; 
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(32, 32);
}

void Coin::UpdateActor(float deltaTime)
{
	if (mCollide->Intersect(GetGame()->GetPlayer()->GetCollisionComponent())) {
		if (Mix_PlayChannel(-1, GetGame()->GetSound("Assets/Coin/coin.wav"), 0) == -1) {
			printf("Mix_PlayChannel: %s\n", Mix_GetError());
			// may be critical error, or maybe just no channels were free.
			// you could allocated another channel in that case...
		}
		SetState(EDead); 
	}
	if (GetPosition().x < GetGame()->GetCameraPos().x)
		SetState(EDead); 
}


Coin::~Coin()
{
}
