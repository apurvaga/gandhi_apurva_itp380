//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include <SDL/SDL_image.h>
#include "Actor.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include <string>
#include "Background.h"
#include "Grid.h"
using namespace std; 

 

// TODO
Game::Game() {

	 
}

bool Game::initialize() {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return 0;
	}


	// Create an application window with the following settings:
	window = SDL_CreateWindow(
		"Platformer",                  // window title
		SDL_WINDOWPOS_UNDEFINED,           // initial x position
		SDL_WINDOWPOS_UNDEFINED,           // initial y position
		WINDOW_W,                               // width, in pixels
		WINDOW_H,                               // height, in pixels
		SDL_WINDOW_OPENGL                  // flags - see below
	);

	// Check that the window was successfully created
	if (window == NULL) {
		// In the case that the window could not be made...
		SDL_Log("Could not create window: %s\n", SDL_GetError());
		return 0;
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if((renderer) == NULL) {
		SDL_Log("Unable to initialize SDL renderer: %s", SDL_GetError());
		return 0;
	}

	IMG_Init(IMG_INIT_PNG);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048);
	loadData();
	currentTime = float(SDL_GetTicks()/1000); 
	return 1;
}

void Game::runLoop() {
	while (!quit) {
		processInput(); 
		updateGame();
		generateOutput(); 
	}
}

void Game::processInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		/* handle event here */
		switch (event.type) {
			case SDL_QUIT:
				quit = 1;
			}
	}
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE]) {
		quit = 1; 
	}
	
	for (Actor* actor : actors) {
		actor->ProcessInput(state);
	}

}

void Game::updateGame()
{
	float deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime; 
	


	//limit frames to 60 Hz
	while (deltaTime * 1000 < 16) {
		deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime;
	}

	currentTime = deltaTime + currentTime;

	//put max limit on delta time for debugging purposes
	deltaTime = (deltaTime > 0.05) ? 0.05 : deltaTime; 



	//cout << deltaTime << endl; //check to see if game runs at 60 Hz

	auto tempActors = actors; 
	vector<Actor*> deadActors; 
	for (auto actor : tempActors) {
		actor->Update(deltaTime);
	}
	
	for (auto actor : actors) {
		if (actor->GetState() == actor->EDead) {
			deadActors.push_back(actor);
		}
	}

	for (auto actor : deadActors) {
		//RemoveActor(actor);
		delete actor; 
	}

	
}

void Game::generateOutput()
{
	SDL_SetRenderDrawColor(renderer, 34, 139, 34, 255);
	//clear
	SDL_RenderClear(renderer);

	//draw game objects
	for (auto sprite : sprites)
		sprite->Draw(renderer); 


	//present
	SDL_RenderPresent(renderer);
}

void Game::loadTexture(std::string fileName)
{
	SDL_Surface* surface = IMG_Load(fileName.c_str());
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	textures[fileName] = texture; 
}

SDL_Texture * Game::GetTexture(std::string name)
{
	return textures.find(name) != textures.end() ? textures[name] : nullptr;
}

/*void Game::readLevel(std::string filename)
{
	ifstream ifile;
	ifile.open(filename);  
	for (int i = 0; i < NUM_BLOCKS_PER_ROW * 24; i++) { 
		char type; 
		ifile.get(type);
		if (type == 'A' || type == 'B' || type == 'C' || type == 'D' || type == 'E' || type == 'F' || type == 'P' || type == '*') {
			if (type == 'P' && player == nullptr) {
				player = new Player(this); 
				initialPlayerPosition = Vector2(nextLevelStartPos + (i % NUM_BLOCKS_PER_ROW) * BLOCK_W + INITIAL_BLOCK_X, (i / NUM_BLOCKS_PER_ROW) * BLOCK_H + INITIAL_BLOCK_Y); 
				player->SetPosition(initialPlayerPosition); 
			}
			else if (type == '*') {
				Coin* coin = new Coin(this);
				coin->SetPosition(Vector2(nextLevelStartPos + (i % NUM_BLOCKS_PER_ROW) * BLOCK_W + INITIAL_BLOCK_X, (i / NUM_BLOCKS_PER_ROW) * BLOCK_H + INITIAL_BLOCK_Y  - COIN_FLOAT));
			}
			else if(type == 'A' || type == 'B' || type == 'C' || type == 'D' || type == 'E' || type == 'F'){
				Block* block = new Block(this, std::string(1, type));
				block->SetPosition(Vector2(nextLevelStartPos + (i % NUM_BLOCKS_PER_ROW) * BLOCK_W + INITIAL_BLOCK_X, (i / NUM_BLOCKS_PER_ROW) * BLOCK_H + INITIAL_BLOCK_Y));
			}
		}
		else if (type != '.') {
			i--;
		}
	}
}*/

void Game::LoadSound(const std::string & filename)
{
	 mix_chunks[filename] = (Mix_LoadWAV(filename.c_str()));
	if (!mix_chunks[filename]) {
		printf("Mix_LoadWAV: %s\n", Mix_GetError());
		// handle error
	}
}

Mix_Chunk * Game::GetSound(const std::string & filename)
{
	return mix_chunks[filename]; 
}

void Game::AddPlane(Plane * plane)
{
	planes.push_back(plane); 
}

void Game::RemovePlane(Plane * plane)
{
	planes.erase(std::remove(planes.begin(), planes.end(), plane), planes.end());
}



void Game::AddActor(Actor* actor) {
	actors.push_back(actor); 
}

void Game::RemoveActor(Actor* actor) {
	actors.erase(std::remove(actors.begin(), actors.end(), actor), actors.end());
}

void Game::AddSprite(SpriteComponent * sprite) {
	sprites.push_back(sprite);

	std::sort(sprites.begin(), sprites.end(),
		[](SpriteComponent* a, SpriteComponent* b) {
		return a->GetDrawOrder() < b->GetDrawOrder();
	});
}


void Game::RemoveSprite(SpriteComponent * sprite)
{
	sprites.erase(std::remove(sprites.begin(), sprites.end(), sprite), sprites.end());
}


void Game::loadData() {

	string directory = "Assets/"; 
	loadTexture(directory + "Airplane.png"); 
	loadTexture(directory + "Bullet.png");
	loadTexture(directory + "TileBrown.png"); 
	loadTexture(directory + "TileBrownSelected.png"); 
	loadTexture(directory + "TileGreen.png");
	loadTexture(directory + "TileGrey.png"); 
	loadTexture(directory + "TileGreySelected.png");
	loadTexture(directory + "TileTan.png");
	loadTexture(directory + "Tower.png"); 

	mGrid = new Grid(this);

	//LoadNextLevel(); 
}

/*void Game::LoadNextLevel() {
	stringstream ss; 
	ss << "Assets/Level"; 
	ss << (currentLevel++); 
	ss << ".txt";
	readLevel(ss.str());
	nextLevelStartPos += LEVEL_WIDTH; 
	currentLevel %= NUM_LEVELS; 
}*/

void Game::unloadData() {
	while (!actors.empty()) {
		delete actors.back(); 
		//actors.pop_back();
	}
	for (auto texturePair : textures) {
		SDL_DestroyTexture(texturePair.second); 
	}
	for (auto chunk : mix_chunks) {
		Mix_FreeChunk(chunk.second); 
	}
}

void Game::shutdown() {
	unloadData(); 
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit(); 
	SDL_Quit();
}



