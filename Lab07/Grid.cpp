#include "Grid.h"
#include "Tile.h"
#include <SDL/SDL.h>
#include <algorithm>
#include <vector>
#include <iostream>
#include "Tower.h"
#include "Plane.h"

using namespace std;

Grid::Grid(class Game* game)
	:Actor(game)
	,mSelectedTile(nullptr)
{
	// 7 rows, 16 columns
	mTiles.resize(NumRows);
	for (size_t i = 0; i < mTiles.size(); i++)
	{
		mTiles[i].resize(NumCols);
	}
	
	// Create tiles
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			mTiles[i][j] = new Tile(GetGame());
			mTiles[i][j]->SetPosition(Vector2(TileSize/2.0f + j * TileSize, StartY + i * TileSize));
		}
	}
	
	// Set start/end tiles
	GetStartTile()->SetTileState(Tile::EStart);
	GetEndTile()->SetTileState(Tile::EBase);
	
	// Set up adjacency lists
	for (size_t i = 0; i < NumRows; i++)
	{
		for (size_t j = 0; j < NumCols; j++)
		{
			if (i > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i-1][j]);
			}
			if (i < NumRows - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i+1][j]);
			}
			if (j > 0)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j-1]);
			}
			if (j < NumCols - 1)
			{
				mTiles[i][j]->mAdjacent.push_back(mTiles[i][j+1]);
			}
		}
	}

	GetEndTile()->mParent = nullptr;
	TryFindPath(); 
	UpdatePathTiles();
	
}

void Grid::SelectTile(size_t row, size_t col)
{
	// Make sure it's a valid selection
	Tile::TileState tstate = mTiles[row][col]->GetTileState();
	if (tstate != Tile::EStart && tstate != Tile::EBase)
	{
		// Deselect previous one
		if (mSelectedTile)
		{
			mSelectedTile->ToggleSelect();
		}
		mSelectedTile = mTiles[row][col];
		mSelectedTile->ToggleSelect();
	}
}

Tile* Grid::GetStartTile()
{
	return mTiles[3][0];
}

Tile* Grid::GetEndTile()
{
	return mTiles[3][15];
}

void Grid::ActorInput(const Uint8 * keyState)
{
	// Process mouse click to select a tile
	int x, y;
	Uint32 buttons = SDL_GetMouseState(&x, &y);
	if (SDL_BUTTON(buttons) & SDL_BUTTON_LEFT)
	{
		// Calculate the x/y indices into the grid
		y -= static_cast<int>(StartY - TileSize / 2);
		if (y >= 0)
		{
			x /= static_cast<int>(TileSize);
			y /= static_cast<int>(TileSize);
			if (x >= 0 && x < static_cast<int>(NumCols) && y >= 0 && y < static_cast<int>(NumRows))
			{
				SelectTile(y, x);
			}
		}
	}
	
	if (!mSpacePressed && keyState[SDL_SCANCODE_SPACE] && mSelectedTile && !mSelectedTile->mBlocked) {
		needToBuild = true;
	}
	else {
		needToBuild = false; 
	}

	mSpacePressed = keyState[SDL_SCANCODE_SPACE]; 
}

void Grid::UpdateActor(float deltaTime)
{
	if (needToBuild) {
		BuildTower(mSelectedTile);
	}
	if (mPlaneTimer < 0) {
		Plane* plane = new Plane(mGame);
		plane->SetPosition(GetStartTile()->GetPosition()); 
		mPlaneTimer = 1.0f; 
	}
	else {
		mPlaneTimer -= deltaTime; 
	}
}

bool Grid::TryFindPath()
{
	for (auto& row : mTiles) {
		for (auto& tile : row) {
			tile->g = 0.0f; 
			tile->mInClosedSet = false; 
		}
	}
	std::vector <Tile*> openSet; 
	Tile* startnode = GetEndTile(); 
	Tile* currentNode = startnode;
	currentNode->mInClosedSet = true; 
	do {
		for (auto& adjacentTile : currentNode->mAdjacent) {
			if (adjacentTile->mInClosedSet)
				continue; 
			else if (std::find(openSet.begin(), openSet.end(), currentNode) != openSet.end()) {
				float new_g = adjacentTile->mParent->g + (adjacentTile->mParent->GetPosition() - adjacentTile->GetPosition()).Length();
				if (new_g < currentNode->g) {
					adjacentTile->mParent = currentNode; 
					adjacentTile->g = new_g; 
					adjacentTile->f = adjacentTile->g + adjacentTile->h; 

				}
			}
			else if(!adjacentTile->mBlocked) {
				adjacentTile->mParent = currentNode; 
				adjacentTile->h = (GetStartTile()->GetPosition() - adjacentTile->GetPosition()).Length(); 
				adjacentTile->g = (GetEndTile()->GetPosition() - adjacentTile->GetPosition()).Length();
				adjacentTile->f = adjacentTile->h + adjacentTile->g; 
				openSet.push_back(adjacentTile); 
			}
		}

		if (openSet.empty()) {
			return false; 
		}

		float minF = openSet[0]->f; 
		Tile* minTile = openSet[0]; 
		for (auto& tile : openSet) {
			if (tile->f < minF) {
				minF = tile->f; 
				minTile = tile; 
			}
		}

		currentNode = minTile; 
		currentNode->mInClosedSet = true; 
		openSet.erase(std::remove(openSet.begin(), openSet.end(), currentNode), openSet.end());

	} while (currentNode != GetStartTile()); 
	return true; 
}

void Grid::UpdatePathTiles() {
	Tile* startTile = GetStartTile();
	Tile* endTile = GetEndTile();
	for (auto& row : mTiles) {
		for (auto& tile : row) {
			if (tile != startTile && tile != endTile)
				tile->SetTileState(Tile::EDefault);
		}
	}

	Tile* currentTile = startTile; 
	while (currentTile != GetEndTile()) {
		if (currentTile != startTile && currentTile != endTile)
			currentTile->SetTileState(Tile::EPath);
		
		currentTile = currentTile->mParent; 
	}

}

void Grid::BuildTower(Tile* tile) {
	tile->mBlocked = true; 
	if(TryFindPath()){
		UpdatePathTiles();
		Tower* newTower = new Tower(mGame); 
		newTower->SetPosition(tile->GetPosition());
	}
	else {
		tile->mBlocked = false; 
		TryFindPath(); 
	}
}

