#include "Bullet.h"
#include "Game.h"
#include "Plane.h"
#include "CollisionComponent.h"



Bullet::Bullet(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this, 200); 
	mSprite->SetTexture(mGame->GetTexture("Assets/Bullet.png")); 
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(10, 10); 
	mMove = new MoveComponent(this); 
	mMove->SetForwardSpeed(500.0f); 
}


Bullet::~Bullet()
{
}

void Bullet::UpdateActor(float deltaTime)
{
	lifeTimer -= deltaTime; 
	if (lifeTimer <= 0) {
		SetState(Actor::EDead);
	}
	else {
		lifeTimer -= deltaTime; 
		auto planes = mGame->GetPlanes(); 
		for (auto plane : planes) {
			if (plane->GetCollisionComponent()->Intersect(mCollide)) {
				SetState(Actor::EDead); 
				plane->SetState(Actor::EDead);
				break;
			}
		}
	}

}
