#pragma once
#include "Actor.h"
class Game; 
class Coin :
	public Actor
{
public:
	Coin(Game* game);
	void UpdateActor(float deltaTime) override; 
	~Coin();
};

