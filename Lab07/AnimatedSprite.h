#pragma once
#include "SpriteComponent.h"
#include <vector>
class AnimatedSprite :
	public SpriteComponent
{
public:
	AnimatedSprite(Actor* actor, int drawOrder);
	~AnimatedSprite();
	void AddImages(SDL_Texture* image) { mImages.push_back(image); }
	void Update(float deltaTime) override;
protected: 
	std::vector<SDL_Texture*> mImages;
	float mAnimTimer = 0.0f; 
	float mAnimSpeed = 15.0f; 
};

