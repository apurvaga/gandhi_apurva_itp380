#include "Plane.h"
#include "Game.h"
#include "PlaneMove.h"
#include "Grid.h"
#include "Tile.h"



Plane::Plane(Game* game)
	: Actor(game)
{
	SetPosition(mGame->GetGrid()->GetStartTile()->GetPosition()); 
	mSprite = new SpriteComponent(this, 200);
	mSprite->SetTexture(mGame->GetTexture("Assets/Airplane.png"));
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(64, 64);
	mMove = new PlaneMove(this); 
	mGame->AddPlane(this);
}


Plane::~Plane()
{
	mGame->RemovePlane(this);
}
