#pragma once
#include "Actor.h"
class Plane; 
class Tower :
	public Actor
{
public:
	Tower(Game* game);
	~Tower();
	void UpdateActor(float deltaTime) override; 

private: 
	float attackTimer = 2.0f; 
	Plane* GetClosestPlane(); 
};

