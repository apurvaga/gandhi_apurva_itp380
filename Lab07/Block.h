#pragma once
#include "Actor.h"
class Block :
	public Actor
{
public:
	void changeTexture(std::string blockType); 
	Block(Game* game, std::string blockType);
	void UpdateActor(float deltaTime) override; 
	~Block();
};

