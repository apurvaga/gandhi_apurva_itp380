#pragma once
#include "Actor.h"
#include "Game.h"
class BarrelSpawner :
	public Actor
{
public:
	BarrelSpawner(Game* game, Vector2 location);
	~BarrelSpawner();
	void UpdateActor(float deltaTime) override; 

protected: 
	float timer = 3.0f; 
	Vector2 mLocation; 
	const float BARREL_SPAWN_DELAY = 3.0f; 
};

