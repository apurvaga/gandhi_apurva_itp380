#pragma once
#include "SpriteComponent.h"
#include <vector>
class Background :
	public SpriteComponent
{
public:
	Background(Actor* actor, int drawOrder, float mParallax);
	~Background();
	void AddImage(SDL_Texture* image) { backgrounds.push_back(image); }
	void Draw(SDL_Renderer* renderer) override;
	float mParallax = 1.0f;
protected: 
	std::vector<SDL_Texture*> backgrounds; 
	
};

