#include "Tower.h"
#include "Game.h"
#include "SpriteComponent.h"
#include "Plane.h"
#include "Bullet.h"


Tower::Tower(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this, 200); 
	mSprite->SetTexture(mGame->GetTexture("Assets/Tower.png"));

}


Tower::~Tower()
{
}

void Tower::UpdateActor(float deltaTime)
{
	if (attackTimer < 0) {
		attackTimer = 2.0f; 
		Plane* plane = GetClosestPlane();
		Vector2 displacement = plane->GetPosition() - mPosition;
		if (displacement.LengthSq() <= 100.0f*100.0f) {
			float theta = atan2(displacement.y, displacement.x);
			this->SetRotation(-theta);
			Bullet* bullet = new Bullet(mGame);
			bullet->SetPosition(mPosition);
			bullet->SetRotation(mRotation);
		}
	}
	else {
		attackTimer -= deltaTime; 
	}
}

Plane* Tower::GetClosestPlane() {
	auto planes = mGame->GetPlanes(); 
	float minDistanceSq = (mPosition - planes[0]->GetPosition()).LengthSq(); 
	Plane* minPlane = planes[0]; 
	for (auto plane : planes) {
		if ((mPosition - plane->GetPosition()).LengthSq() < minDistanceSq) {
			minPlane = plane; 
			minDistanceSq = (mPosition - plane->GetPosition()).LengthSq();
		}
	}
	return minPlane; 
}


