#include "PlaneMove.h"
#include "Actor.h"
#include "Tile.h"
#include "Game.h"
#include "Grid.h"
#include "CollisionComponent.h"



PlaneMove::PlaneMove(Actor* actor) 
	: MoveComponent(actor)
{
	SetForwardSpeed(200.0f); 
	SetNextTile(mOwner->GetGame()->GetGrid()->GetStartTile()->GetParent());
}


PlaneMove::~PlaneMove()
{
}

void PlaneMove::Update(float deltaTime)
{
	MoveComponent::Update(deltaTime); 


	//changed the < condition from 2.0 to 7.0 because the SetNextTile() function was not being triggered sometimes when it was kept 2.0
	if (mNextTile && (mNextTile->GetPosition() - mOwner->GetPosition()).Length() <= 7.0f) {
		SetNextTile(mNextTile->GetParent());
	}

	if (mOwner->GetPosition().x >= mOwner->GetGame()->WINDOW_W)
		mOwner->SetState(Actor::EDead);
	

}

void PlaneMove::SetNextTile(const Tile* tile)
{
	mNextTile = tile; 
	if (!mNextTile)
		mOwner->SetRotation(0.0f); 
	else {
		Vector2 displacement = mOwner->GetPosition() - tile->GetPosition();
		float theta = atan2(displacement.y, displacement.x);

		//since the tiles are square shaped use M_PI/4 

		//right
		if (theta <= M_PI / 4 && theta >= -M_PI / 4) {
			mOwner->SetRotation(M_PI);
		}

		//left
		else if ((theta <= -3 * M_PI / 4 && theta >= -M_PI) || (theta >= 3 * M_PI / 4 && theta <= M_PI)) {
			mOwner->SetRotation(0.0f);
		}

		//down
		else if (theta < 3 * M_PI / 4 && theta > M_PI / 4) {
			mOwner->SetRotation(M_PI / 2);
		}

		//up
		else if (theta <= -M_PI / 4 && theta >= -3 * M_PI / 4) {
			mOwner->SetRotation(-M_PI / 2);
		}
	}
}
