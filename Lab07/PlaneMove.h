#pragma once
#include "MoveComponent.h"
class Tile; 
class PlaneMove :
	public MoveComponent
{
public:
	PlaneMove(Actor* actor);
	~PlaneMove();
	void Update(float deltaTime) override; 
	void SetNextTile(const Tile* tile); 

private: 
	const Tile * mNextTile = nullptr; 
};

