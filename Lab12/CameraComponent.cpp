#include "CameraComponent.h"
#include "Game.h"
#include "Renderer.h"
#include "Actor.h"
#include "Math.h"



CameraComponent::CameraComponent(Actor* actor)
	:Component(actor)
{
}


CameraComponent::~CameraComponent()
{
}

void CameraComponent::Update(float deltaTime)
{
	
	mPitchAngle += deltaTime * mPitchSpeed; 
	if (mPitchAngle > Math::PiOver4)
		mPitchAngle = Math::PiOver4; 
	else if (mPitchAngle < -Math::PiOver4)
		mPitchAngle = -Math::PiOver4;

	Matrix4 yaw = Matrix4::CreateRotationZ(mOwner->GetRotation());
	Matrix4 pitch = Matrix4::CreateRotationY(mPitchAngle);
	Matrix4 rotation = pitch* yaw; 
	Vector3 cameraForward = Vector3::Transform(Vector3(1, 0, 0), rotation);
	mOwner->GetGame()->GetRenderer()->SetViewMatrix(Matrix4::CreateLookAt(mOwner->GetPosition(), mOwner->GetPosition() + cameraForward, Vector3(0, 0, 1)));

}
