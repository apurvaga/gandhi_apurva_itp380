#pragma once
#include "Math.h"
#include <string>
#include <cmath>
#include <algorithm>

class HUD
{
public:
	HUD(class Game* game);
	~HUD();
	// UIScreen subclasses can override these
	virtual void Update(float deltaTime);
	virtual void Draw(class Shader* shader);
	void DecrementCoinCounter() { mCoinCounter = std::max(0, (int) mCoinCounter - 1); }
	void UpdateCheckpointText(class Checkpoint* checkpoint);
protected:
	// Helper to draw a texture
	void DrawTexture(class Shader* shader, class Texture* texture,
			const Vector2& offset = Vector2::Zero, float scale = 1.0f);
	

	class Game* mGame;
	
	class Font* mFont;
	// TODO: Add textures/member variables
	Texture* mTimerText; 
	float mTimerCount = 0.0f; 
	unsigned int mCoinCounter = 55; 
	Texture* mCoinText;  
	Texture* mCheckpointText;
	float mCheckpointCounter = 5.0f; 
};
