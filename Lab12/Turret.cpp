#include "Turret.h"
#include "Mesh.h"
#include "Renderer.h"
#include "Game.h"
#include <iostream>


Turret::Turret(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this); 
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/TankTurret.gpmesh")); 
	mMove = new MoveComponent(this);
	mScale = 1.8f; 
	mClockwise = SDL_SCANCODE_E; 
	mCounterClockwise = SDL_SCANCODE_Q; 
}

void Turret::ActorInput(const Uint8 * keyState)
{
	
	mMove->SetAngularSpeed(0);
	if (keyState[mCounterClockwise]) {
		mMove->SetAngularSpeed(Math::TwoPi);
	}
		
	else if (keyState[mClockwise]) {
		mMove->SetAngularSpeed(-Math::TwoPi);
	}
}

void Turret::SetPlayerTwo()
{
	mMesh->SetTextureIndex(1); 
	mCounterClockwise = SDL_SCANCODE_I; 
	mClockwise = SDL_SCANCODE_P; 
}


Turret::~Turret()
{
}
