#pragma once
#include "Actor.h"
class Checkpoint :
	public Actor
{
public:
	Checkpoint(Game* game);
	~Checkpoint();
	void Activate() { active = true;  }
	void Deactivate() { active = false;  }
	void UpdateActor(float deltaTime) override;
	bool IsActive() { return active;  }
	void SetLevelString(std::string level) { mLevelString = level; }
	void SetDisplayText(std::string text) { mDisplayText = text; }
	std::string GetDisplayText() { return mDisplayText; }

private: 
	bool active = false; 
	std::string mLevelString; 
	std::string mDisplayText;
};

