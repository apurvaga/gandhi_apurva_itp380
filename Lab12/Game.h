#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include <unordered_map>
#include <string>
#include <vector>
#include "Math.h"
#include <queue>
class Tank; 
class Block; 
class Game
{
public:
	Game();
	bool Initialize();
	void RunLoop();
	void Shutdown();

	void AddActor(class Actor* actor);
	void RemoveActor(class Actor* actor);

	void LoadSound(const std::string& fileName);
	Mix_Chunk* GetSound(const std::string& fileName);

	void LoadLevel(const std::string& fileName);

	class Renderer* GetRenderer() {	return mRenderer; }

	void AddBlock(Block* block);
	void RemoveBlock(Block * block);
	std::vector<Block*> GetBlocks() { return blocks; }
	void AddCheckPoint(class Checkpoint* checkpoint) {
		checkpoints.push(checkpoint); 
	}
	class Checkpoint* GetActiveCheckpoint() { return checkpoints.size() > 0 ? checkpoints.front() : nullptr; }
	class Player* GetPlayer() { return mPlayer; }
	void SetPlayer(class Player* player) { mPlayer = player;  }
	std::queue<class Checkpoint*>& GetCheckpoints() { return checkpoints; }
	void SetNextLevel(std::string level) { mNextLevel = level; }
	void LoadNextLevel();
	class HUD* GetHUD() { return mHUD;  }

private:
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();
	void LoadData();
	void UnloadData();

	// Map of textures loaded
	std::unordered_map<std::string, SDL_Texture*> mTextures;
	std::unordered_map<std::string, Mix_Chunk*> mSounds;

	// All the actors in the game
	std::vector<class Actor*> mActors;

	class Renderer* mRenderer;

	Uint32 mTicksCount;
	bool mIsRunning;
	std::vector<Block*> blocks; 
	std::queue<class Checkpoint*> checkpoints; 
	Player* mPlayer; 
	std::string mNextLevel; 
	class HUD* mHUD;
};
