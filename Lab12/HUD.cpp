#include "HUD.h"
#include "Texture.h"
#include "Shader.h"
#include "Game.h"
#include "Renderer.h"
#include "Font.h"
#include <sstream>
#include <iomanip>
#include "Checkpoint.h"

HUD::HUD(Game* game)
	:mGame(game)
	,mFont(nullptr)
{
	// Load font
	mFont = new Font();
	mFont->Load("Assets/Inconsolata-Regular.ttf");

	mTimerText = mFont->RenderText("00:00.00");	mCoinText = mFont->RenderText("55/55");	//mCheckpointText =  mFont->RenderText("Hi");
}

HUD::~HUD()
{
	// Get rid of font
	if (mFont)
	{
		mFont->Unload();
		delete mFont;
	}
}

void HUD::Update(float deltaTime)
{
	// TODO
	mTimerCount += deltaTime; 
	mCheckpointCounter -= deltaTime; 
	mTimerText->Unload(); 
	delete mTimerText;

	unsigned int r, m, s, f;

	r = (unsigned int) (mTimerCount * 1000.0f);
	m = r / 60000;
	r = r % 60000;
	s = r / 1000;
	f = r % 1000;
	f /= 10;

	char str[128];

	sprintf_s(str, "%02d:%02d.%02d", m, s, f);

	mTimerText = mFont->RenderText(str); 

	sprintf_s(str, "%02d/55", mCoinCounter);

	mCoinText = mFont->RenderText(str);


}

void HUD::Draw(Shader* shader)
{
	// TODO
	DrawTexture(shader, mTimerText, Vector2(-420.0f, -325.0f));
	DrawTexture(shader, mCoinText, Vector2(-440.0f, -295.0f));
	if(mCheckpointCounter > 0)
		DrawTexture(shader, mCheckpointText, Vector2::Zero);

}


void HUD::DrawTexture(class Shader* shader, class Texture* texture,
				 const Vector2& offset, float scale)
{
	// Scale the quad by the width/height of texture
	Matrix4 scaleMat = Matrix4::CreateScale(
		static_cast<float>(texture->GetWidth()) * scale,
		static_cast<float>(texture->GetHeight()) * scale,
		1.0f);
	// Translate to position on screen
	Matrix4 transMat = Matrix4::CreateTranslation(
		Vector3(offset.x, offset.y, 0.0f));	
	// Set world transform
	Matrix4 world = scaleMat * transMat;
	shader->SetMatrixUniform("uWorldTransform", world);
	// Set current texture
	texture->SetActive();
	// Draw quad
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, nullptr);
}

void HUD::UpdateCheckpointText(Checkpoint* checkpoint)
{
	mCheckpointText = mFont->RenderText(checkpoint->GetDisplayText());
	mCheckpointCounter = 5.0f; 
}
