#pragma once
#include <vector>
#include <SDL/SDL_stdinc.h>
#include "Math.h"
#include "MoveComponent.h"
#include "CollisionComponent.h"
#include "MeshComponent.h"
class Actor
{
public:
	enum State
	{
		EActive,
		EPaused,
		EDead
	};
	
	Actor(class Game* game);
	virtual ~Actor();

	// Update function called from Game (not overridable)
	void Update(float deltaTime);
	// Any actor-specific update code (overridable)
	virtual void UpdateActor(float deltaTime);
	// ProcessInput function called from Game (not overridable)
	void ProcessInput(const Uint8* keyState);
	// Any actor-specific update code (overridable)
	virtual void ActorInput(const Uint8* keyState);

	// Getters/setters
	const Vector3& GetPosition() const { return mPosition; }
	void SetPosition(const Vector3& pos) { mPosition = pos; }
	float GetScale() const { return mScale; }
	void SetScale(float scale) { mScale = scale; }
	float GetRotation() const { return mRotation; }
	void SetRotation(float rotation) { mRotation = rotation; }
	
	State GetState() const { return mState; }
	void SetState(State state) { mState = state; }

	class Game* GetGame() { return mGame; }

	MeshComponent* GetMesh() { return mMesh;  }

	Vector3 GetForward()  const;
	Vector3 GetRight() const;

	CollisionComponent* GetCollisionComponent() { return mCollide; }
	MoveComponent* GetMoveComponent() { return mMove; }
	const Matrix4& GetWorldTransform() const { return mWorldTransform; }
	class CameraComponent* GetCamera() { return mCamera; }
	Quaternion GetQuaternion() { return mQuat; }
	void SetQuaternion(Quaternion quat) { mQuat = quat; }



protected:
	class Game* mGame;
	// Actor's state
	State mState;
	// Transform
	Vector3 mPosition;
	float mScale;
	float mRotation;
	MoveComponent* mMove; 
	CollisionComponent* mCollide; 
	Matrix4 mWorldTransform; 
	MeshComponent* mMesh; 
	class CameraComponent* mCamera; 
	Quaternion mQuat; 
};
