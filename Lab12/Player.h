#pragma once
#include "Actor.h"
class Player :
	public Actor
{
public:
	Player(Game* game);
	Vector3 GetRespawnPos() { return pos; }
	void SetRespawnPos(Vector3 pos) { this->pos = pos;  }
	~Player();

private: 
	Vector3 pos; 
};

