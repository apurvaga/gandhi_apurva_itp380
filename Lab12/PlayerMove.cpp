#include "PlayerMove.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "CameraComponent.h"
#include <algorithm>
#include "Block.h"
#include "Game.h"
#include <iostream>
#include "Player.h"



PlayerMove::PlayerMove(Actor* actor)
	: MoveComponent(actor)
{
	mRunningSFX = Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Running.wav"), -1);
	Mix_Pause(mRunningSFX);
	ChangeState(Falling);
}


PlayerMove::~PlayerMove()
{
	Mix_HaltChannel(mRunningSFX);
}

void PlayerMove::Update(float deltaTime)
{
	switch(mCurrentState) {
		case Falling: 
			UpdateFalling(deltaTime);
			break;
		case Jump: 
			UpdateJump(deltaTime);
			break;
		case OnGround: 
			UpdateOnGround(deltaTime);
			break;
		case WallClimb:
			UpdateWallClimb(deltaTime); 
			break; 
		case WallRun: 
			UpdateWallRun(deltaTime);
			break;
 
	}
	if (mOwner->GetPosition().z < -750.0f)
		Respawn();
	if ((mCurrentState == OnGround && mVelocity.LengthSq() >= 50.0f*50.0f)
		|| mCurrentState == WallClimb || mCurrentState == WallRun)
		Mix_Resume(mRunningSFX);
	else
		Mix_Pause(mRunningSFX);
}

void PlayerMove::ProcessInput(const Uint8 * keyState)
{
	//forward
	if (keyState[SDL_SCANCODE_W] && !keyState[SDL_SCANCODE_S])
		AddForce(mOwner->GetForward()*700.0f); 
	else if (keyState[SDL_SCANCODE_S] && !keyState[SDL_SCANCODE_W])
		AddForce(mOwner->GetForward()*-700.0f);
	

	//strafe
	if (keyState[SDL_SCANCODE_D] && !keyState[SDL_SCANCODE_A])
		AddForce(mOwner->GetRight()*700.0f);
	else if (keyState[SDL_SCANCODE_A] && !keyState[SDL_SCANCODE_D])
		AddForce(mOwner->GetRight()*-700.0f);


	//mouse
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);
	float x_movement = x / 500.0f; 
	SetAngularSpeed(Math::Pi*10.0f*x_movement);

	float y_movement = y / 500.0f; 
	mOwner->GetCamera()->SetPitchSpeed(Math::Pi*10.0f*y_movement);

	if (keyState[jumpKey] && !jumpKeyPressed && mCurrentState == OnGround) {
		Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Jump.wav"), 0);
		AddForce(mJumpForce);
		ChangeState(Jump);
	}
		
	jumpKeyPressed = keyState[jumpKey];

}

void PlayerMove::ChangeState(MoveState state)
{
	mCurrentState = state; 
}

void PlayerMove::UpdateOnGround(float deltaTime)
{
	PhysicsUpdate(deltaTime);
	ChangeState(Falling);
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		CollSide side = FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()); 
		if (side == SideX1 || side == SideX2 || side == SideY1 || side == SideY2) {
			if (CanWallClimb(side)) {
				mWallClimbTimer = 0.0f; 
				mCurrentState = WallClimb;
				return;
			}
		}
		if ( side == Top) {
			ChangeState(OnGround);
			//break; 
		}

	}
}

void PlayerMove::UpdateJump(float deltaTime)
{
	AddForce(mGravity);
	PhysicsUpdate(deltaTime);
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		CollSide side = FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent());
		if (side == SideX1 || side == SideX2 || side == SideY1 || side == SideY2) {
			if (CanWallClimb(side)) {
				mWallClimbTimer = 0.0f;
				mCurrentState = WallClimb;
				return;
			}
			else if (CanWallRun(side)) {
				mWallRunTimer = 0.0f; 
				mCurrentState = WallRun; 
				return; 
			}
		}
		if (side == Bottom) {
			mVelocity.z = 0.0f;
			break; 
		}

	}
	if (mVelocity.z <= 0.0f)
		ChangeState(Falling);
}

void PlayerMove::UpdateFalling(float deltaTime)
{
	AddForce(mGravity);
	PhysicsUpdate(deltaTime);
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()) == Top) {
			Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Sounds/Land.wav"), 0);
			mVelocity.z = 0.0f;
			ChangeState(OnGround);
		}

	}
}

void PlayerMove::UpdateWallClimb(float deltaTime)
{
	mWallClimbTimer += deltaTime; 
	AddForce(mGravity); 
	if(mWallClimbTimer <= 0.4f)
		AddForce(mWallClimbForce);
	PhysicsUpdate(deltaTime); 
	auto blocks = mOwner->GetGame()->GetBlocks();
	bool collided = false; 
	for (auto block : blocks) {
		CollSide side = FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()); 
		if (side == SideX1 || side == SideX2 || side == SideY1 || side == SideY2) {
			collided = true; 
		}

	}
	if (!collided || mVelocity.z <= 0.0f) {
		mVelocity.z = 0.0f; 
		mCurrentState = Falling;
	}

}

void PlayerMove::UpdateWallRun(float deltaTime)
{
	mWallRunTimer += deltaTime;
	AddForce(mGravity);
	if (mWallRunTimer <= 0.4f)
		AddForce(mWallRunForce);
	PhysicsUpdate(deltaTime);
	auto blocks = mOwner->GetGame()->GetBlocks();
	bool collided = false;
	for (auto block : blocks) {
		CollSide side = FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent());
		if (side == SideX1 || side == SideX2 || side == SideY1 || side == SideY2) {
			collided = true;
		}

	}
	if (mVelocity.z <= 0.0f) {
		mVelocity.z = 0.0f;
		mCurrentState = Falling;
	}
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent * self, CollisionComponent * block)
{
	if (mOwner->GetCollisionComponent()->Intersect(block)) {

		Vector3 blockMax = block->GetMax();
		Vector3 blockMin = block->GetMin();
		Vector3 playerMax = mOwner->GetCollisionComponent()->GetMax();
		Vector3 playerMin = mOwner->GetCollisionComponent()->GetMin();

		float dUp = Math::Abs(playerMax.y - blockMin.y);
		float dDown = Math::Abs(playerMin.y - blockMax.y);
		float dRight = Math::Abs(playerMin.x - blockMax.x);
		float dLeft = Math::Abs(playerMax.x - blockMin.x);
		float dTop = Math::Abs(playerMin.z - blockMax.z); 
		float dBottom = Math::Abs(playerMax.z - blockMin.z);


		float min = std::min(dUp, std::min(dDown, std::min(dRight, std::min(dLeft, std::min(dBottom, dTop)))));

		Vector3 position = mOwner->GetPosition();

		//right side
		if (min == dRight) {
			mOwner->SetPosition(position + Vector3(dRight, 0, 0));
			AddForce(Vector3(700.0f, 0.0f, 0.0f));
			return SideX1; 
		}

		//left side
		else if (min == dLeft) {
			mOwner->SetPosition(position + Vector3(-dLeft, 0, 0));
			AddForce(Vector3(-700.0f, 0.0f, 0.0f));
			return SideX2; 
		}

		//down side
		else if (min == dDown) {
			mOwner->SetPosition(position + Vector3(0, dDown, 0));
			AddForce(Vector3(0.0f, 700.0f, 0.0f));
			return SideY2; 
		}

		//up side
		else if (min == dUp) {
			mOwner->SetPosition(position + Vector3(0, -dUp, 0));
			AddForce(Vector3(0.0f, -700.0f, 0.0f));
			return SideY1; 
		}

		//top 
		else if (min == dTop) {
			mOwner->SetPosition(position + Vector3(0, 0, dTop)); 
			return Top; 
		}

		//bottom 
		else if (min == dBottom) {
			mOwner->SetPosition(position + Vector3(0, 0, -dBottom)); 
			return Bottom; 
		}

	}
	else
		return None; 
}

void PlayerMove::PhysicsUpdate(float deltaTime)
{
	mAcceleration = mPendingForces * (1.0f / mMass); 
	mVelocity += mAcceleration * deltaTime; 
	FixXYVelocity(); 
	Vector3 deltaPosition = mVelocity * deltaTime; 
	mOwner->SetPosition(mOwner->GetPosition() + deltaPosition);
	mOwner->SetRotation(mOwner->GetRotation() + GetAngularSpeed() * deltaTime);
	mPendingForces = Vector3::Zero; 


}

void PlayerMove::AddForce(const Vector3 & force)
{
	mPendingForces += force; 
}

void PlayerMove::FixXYVelocity()
{
	Vector2 xyVelocity = Vector2(mVelocity.x, mVelocity.y); 
	if(xyVelocity.LengthSq() > maxSpeed*maxSpeed)
		xyVelocity = xyVelocity * (maxSpeed /xyVelocity.Length()); 
	if (mCurrentState == OnGround || mCurrentState == WallClimb) {
		if(Math::NearZero(mAcceleration.x) || (mVelocity.x* mAcceleration.x < 0))
			xyVelocity.x *= 0.9f; 
		if (Math::NearZero(mAcceleration.y) || (mVelocity.y * mAcceleration.y < 0))
			xyVelocity.y *= 0.9f;

	}
	mVelocity.x = xyVelocity.x; 
	mVelocity.y = xyVelocity.y; 

}

bool PlayerMove::CanWallClimb(CollSide side)
{
	bool faceCondition = false; 
	bool velocityDirectionCondition = false; 
	bool velocityMagnitudeCondition = false; 

	float faceDot, velocityDot; 
	Vector3 forward = mOwner->GetForward();
	Vector3 xyVelocity = Vector3(mVelocity.x, mVelocity.y, 0); 
	float xySpeed = xyVelocity.Length();
	xyVelocity *= 1/xySpeed; 
	
	switch (side) {
	case SideX1:
		faceDot = Vector3::Dot(forward, Vector3(1, 0, 0));
		velocityDot = Vector3::Dot(xyVelocity, Vector3(1, 0, 0)); 
		break;
	case SideX2:
		faceDot = Vector3::Dot(forward, Vector3(-1, 0, 0));
		velocityDot = Vector3::Dot(xyVelocity, Vector3(-1, 0, 0));
		break;
	case SideY1:
		faceDot = Vector3::Dot(forward, Vector3(0, -1, 0));
		velocityDot = Vector3::Dot(xyVelocity, Vector3(0, -1, 0));
		break;
	case SideY2:
		faceDot = Vector3::Dot(forward, Vector3(0, 1, 0));
		velocityDot = Vector3::Dot(xyVelocity, Vector3(0, 1, 0));
		break;
	}

	faceCondition = faceDot <=  -0.6; 
	velocityDirectionCondition = velocityDot <=  -0.7; 
	velocityMagnitudeCondition = xySpeed >= 0.8*maxSpeed; 

	return faceCondition && velocityDirectionCondition && velocityMagnitudeCondition;
}

bool PlayerMove::CanWallRun(CollSide side)
{
	bool faceCondition = false;
	bool velocityDirectionCondition = false;
	bool velocityMagnitudeCondition = false;

	float faceDot, velocityDot;
	Vector3 forward = mOwner->GetForward();
	Vector3 xyVelocity = Vector3(mVelocity.x, mVelocity.y, 0);
	float xySpeed = xyVelocity.Length();
	xyVelocity *= 1 / xySpeed;


	velocityDot = Vector3::Dot(forward, xyVelocity); 

	switch (side) {
	case SideX1:
		faceDot = Vector3::Dot(forward, Vector3(1, 0, 0));
		break;
	case SideX2:
		faceDot = Vector3::Dot(forward, Vector3(-1, 0, 0));
		break;
	case SideY1:
		faceDot = Vector3::Dot(forward, Vector3(0, -1, 0));
		break;
	case SideY2:
		faceDot = Vector3::Dot(forward, Vector3(0, 1, 0));
		break;
	}
	faceCondition = abs(faceDot) <= 0.3;
	velocityDirectionCondition = velocityDot > 0.2;
	velocityMagnitudeCondition = xySpeed >= 0.875*maxSpeed;

	return faceCondition && velocityDirectionCondition && velocityMagnitudeCondition;
}

void PlayerMove::Respawn()
{
	mOwner->SetPosition(((Player*) mOwner)->GetRespawnPos()); 
	mOwner->SetRotation(0); 
	mVelocity = Vector3::Zero; 
	mPendingForces = Vector3::Zero; 
	mCurrentState = Falling; 
}
