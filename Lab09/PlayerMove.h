#pragma once
#include "MoveComponent.h"
#include <SDL/SDL.h>
class PlayerMove :
	public MoveComponent
{
public:
	enum MoveState {
		OnGround, Jump, Falling
	};
	enum CollSide {
		None, Top, Bottom, Side
	};
	PlayerMove(Actor* actor);
	~PlayerMove();
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8 * keyState) override; 
	void ChangeState(MoveState state); 

protected: 
	void UpdateOnGround(float deltaTime);
	void UpdateJump(float deltaTime);
	void UpdateFalling(float deltaTime); 
	CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
	MoveState mCurrentState; 
	float mZSpeed = 0.0f; 
	const float Gravity = -980.0f; 
	const float JumpSpeed = 500.0f; 
	const unsigned char jumpKey = SDL_SCANCODE_SPACE; 
	bool jumpKeyPressed = false; 

};

