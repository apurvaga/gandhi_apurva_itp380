#include "PlayerMove.h"
#include "Actor.h"
#include "SDL/SDL.h"
#include "CameraComponent.h"
#include <algorithm>
#include "Block.h"
#include "Game.h"



PlayerMove::PlayerMove(Actor* actor)
	: MoveComponent(actor)
{
	ChangeState(Falling);
}


PlayerMove::~PlayerMove()
{
}

void PlayerMove::Update(float deltaTime)
{
	switch(mCurrentState) {
		case Falling: 
			UpdateFalling(deltaTime);
		case Jump: 
			UpdateJump(deltaTime);
		case OnGround: 
			UpdateOnGround(deltaTime);
	}
}

void PlayerMove::ProcessInput(const Uint8 * keyState)
{
	//forward
	if (keyState[SDL_SCANCODE_W])
		SetForwardSpeed(350.0f);
	else if (keyState[SDL_SCANCODE_S])
		SetForwardSpeed(-350.0f);
	
	if (keyState[SDL_SCANCODE_W] && keyState[SDL_SCANCODE_S])
		SetForwardSpeed(0.0f);
	else if (!keyState[SDL_SCANCODE_W] && !keyState[SDL_SCANCODE_S])
		SetForwardSpeed(0.0f);

	//strafe
	if (keyState[SDL_SCANCODE_D])
		SetStrafeSpeed(350.0f);
	else if (keyState[SDL_SCANCODE_A])
		SetStrafeSpeed(-350.0f);

	if (keyState[SDL_SCANCODE_D] && keyState[SDL_SCANCODE_A])
		SetStrafeSpeed(0.0f);
	else if (!keyState[SDL_SCANCODE_D] && !keyState[SDL_SCANCODE_A])
		SetStrafeSpeed(0.0f);

	//mouse
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);
	float x_movement = x / 500.0f; 
	SetAngularSpeed(Math::Pi*10.0f*x_movement);

	float y_movement = y / 500.0f; 
	mOwner->GetCamera()->SetPitchSpeed(Math::Pi*10.0f*y_movement);

	if (keyState[jumpKey] && !jumpKeyPressed && mCurrentState == OnGround) {
		mZSpeed = JumpSpeed; 
		ChangeState(Jump);
	}
		
	jumpKeyPressed = keyState[jumpKey];

}

void PlayerMove::ChangeState(MoveState state)
{
	mCurrentState = state; 
}

void PlayerMove::UpdateOnGround(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	ChangeState(Falling);
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()) == Top) {
			ChangeState(OnGround);
			break; 
		}

	}
}

void PlayerMove::UpdateJump(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	mZSpeed += deltaTime * Gravity;
	mOwner->SetPosition(Vector3(0, 0, mZSpeed)*deltaTime + mOwner->GetPosition());
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()) == Bottom) {
			mZSpeed = 0.0f;
			break; 
		}

	}
	if (mZSpeed <= 0.0f)
		ChangeState(Falling);
}

void PlayerMove::UpdateFalling(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	mZSpeed += deltaTime * Gravity;
	mOwner->SetPosition(Vector3(0, 0, mZSpeed)*deltaTime + mOwner->GetPosition());
	auto blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (FixCollision(mOwner->GetCollisionComponent(), block->GetCollisionComponent()) == Top) {
			mZSpeed = 0.0f;
			ChangeState(OnGround);
		}

	}
}

PlayerMove::CollSide PlayerMove::FixCollision(CollisionComponent * self, CollisionComponent * block)
{
	if (mOwner->GetCollisionComponent()->Intersect(block)) {

		Vector3 blockMax = block->GetMax();
		Vector3 blockMin = block->GetMin();
		Vector3 playerMax = mOwner->GetCollisionComponent()->GetMax();
		Vector3 playerMin = mOwner->GetCollisionComponent()->GetMin();

		float dUp = Math::Abs(playerMax.y - blockMin.y);
		float dDown = Math::Abs(playerMin.y - blockMax.y);
		float dRight = Math::Abs(playerMin.x - blockMax.x);
		float dLeft = Math::Abs(playerMax.x - blockMin.x);
		float dTop = Math::Abs(playerMin.z - blockMax.z); 
		float dBottom = Math::Abs(playerMax.z - blockMin.z);


		float min = std::min(dUp, std::min(dDown, std::min(dRight, std::min(dLeft, std::min(dBottom, dTop)))));

		Vector3 position = mOwner->GetPosition();

		//right side
		if (min == dRight) {
			mOwner->SetPosition(position + Vector3(dRight, 0, 0));
			return Side; 
		}

		//left side
		else if (min == dLeft) {
			mOwner->SetPosition(position + Vector3(-dLeft, 0, 0));
			return Side; 
		}

		//down side
		else if (min == dDown) {
			mOwner->SetPosition(position + Vector3(0, dDown, 0));
			return Side; 
		}

		//up side
		else if (min == dUp) {
			mOwner->SetPosition(position + Vector3(0, -dUp, 0));
			return Side; 
		}

		//top 
		else if (min == dTop) {
			mOwner->SetPosition(position + Vector3(0, 0, dTop)); 
			return Top; 
		}

		//bottom 
		else if (min == dBottom) {
			mOwner->SetPosition(position + Vector3(0, 0, -dBottom)); 
			return Bottom; 
		}

	}
	else
		return None; 
}
