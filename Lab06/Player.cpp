#include "Player.h"
#include "Game.h"
#include "PlayerMove.h"
#include "AnimatedSprite.h"


Player::Player(Game* game)
	: Actor(game)
{
	AnimatedSprite* sprite = new AnimatedSprite(this, 200);
	sprite->AddImages(game->getTexture("Assets/Player/Run1.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run2.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run3.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run4.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run5.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run6.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run7.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run8.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run9.png"));
	sprite->AddImages(game->getTexture("Assets/Player/Run10.png"));
	mSprite = sprite;
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(20, 64);
	mMove = new PlayerMove(this); 
}


Player::~Player()
{
}
