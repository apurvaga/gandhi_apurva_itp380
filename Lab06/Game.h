#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include "Block.h"
#include "Player.h"
#include "SDL/SDL_mixer.h"


class Actor; 
class Paddle; 
class SpriteComponent; 
// TODO
class Game {
	public:
		//constructor
		Game();

		bool initialize(); 

		void shutdown(); 

		void runLoop(); 

		void AddActor(Actor* actor);
		void RemoveActor(Actor* actor);
		void AddSprite(SpriteComponent* sprite);
		void AddBlock(Block* block);
		void RemoveSprite(SpriteComponent* sprite);
		void RemoveBlock(Block * block);
		void loadData();
		void unloadData(); 
		SDL_Texture* getTexture(std::string);

		void readLevel(std::string filename); 

		std::vector<Block*> GetBlocks() { return blocks;  }
		Player* GetPlayer() { return player; }
		Vector2 GetInitialPlayerPosition() { return initialPlayerPosition; }
		void LoadSound(const std::string& filename);
		Mix_Chunk* GetSound(const std::string& filename);
		
		const Vector2& GetCameraPos() { return mCameraPos; }
		void SetCameraPos(const Vector2& cameraPos) { mCameraPos = cameraPos; }

		void LoadNextLevel(); 

		const unsigned int WINDOW_W = 1024;
		const unsigned int WINDOW_H = 768;
		const unsigned int NUM_BLOCKS_PER_ROW = 56; 
		const unsigned int BLOCK_W = 64; 
		const unsigned int BLOCK_H = 32; 
		const unsigned int INITIAL_BLOCK_X = 32; 
		const unsigned int INITIAL_BLOCK_Y = 16; 
		const float GRAVITY = 2000.0f; 
		const unsigned int NUM_LEVELS = 4;
		const unsigned int LEVEL_WIDTH = BLOCK_W * NUM_BLOCKS_PER_ROW; 
		const unsigned int COIN_FLOAT = 16; 



	private:
		SDL_Window * window;
		SDL_Renderer *renderer;
		std::vector<Actor*> actors;
		std::vector<SpriteComponent*> sprites; 
		SDL_Texture* background; 
		std::unordered_map<std::string, SDL_Texture*> textures; 
		float currentTime; 
		char direction = 0; 
		bool quit = false; 
		void processInput(); 
		void updateGame(); 
		void generateOutput(); 
		void loadTexture(std::string); 
		std::vector<Block*> blocks; 
		Player* player = nullptr; 
		Vector2 initialPlayerPosition; 
		std::unordered_map<std::string, Mix_Chunk*> mix_chunks; 
		Vector2 mCameraPos = Vector2(0, 0); 
		int currentLevel = 0; 
		int nextLevelStartPos = 0; 


		
		
		

	



		
};