#include "AnimatedSprite.h"



AnimatedSprite::AnimatedSprite(Actor* actor, int drawOrder)
	:SpriteComponent(actor, drawOrder)
{
}


AnimatedSprite::~AnimatedSprite()
{
}

void AnimatedSprite::Update(float deltaTime)
{
	mAnimTimer += mAnimSpeed * deltaTime; 
	int currentFrame = int(mAnimTimer);
	SetTexture(mImages[currentFrame % mImages.size()]);
}
