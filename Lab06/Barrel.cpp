#include "Barrel.h"
#include "BarrelMove.h"


Barrel::Barrel(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this, 150); 
	mSprite->SetTexture(mGame->getTexture("Assets/Barrel.png")); 
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(32, 32);
	mMove = new BarrelMove(this); 
	

}


Barrel::~Barrel()
{
}
