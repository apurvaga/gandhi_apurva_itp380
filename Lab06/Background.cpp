#include "Background.h"
#include "Actor.h"
#include "Game.h"
#include <iostream>



Background::Background(Actor* actor, int drawOrder, float parallax)
	:SpriteComponent(actor, drawOrder)
{
	mParallax = parallax;
}


Background::~Background()
{
}

void Background::Draw(SDL_Renderer * renderer)
{
	int imageNum = 0; 
	int start = -512;
	while (start - mParallax*mOwner->GetGame()->GetCameraPos().x < 1024) {
		this->SetTexture(backgrounds[imageNum]);
		imageNum = ++imageNum % backgrounds.size(); 
		SDL_Rect r;
		r.w = static_cast<int>(mTexWidth * mOwner->GetScale());
		r.h = static_cast<int>(mTexHeight * mOwner->GetScale());
		// Center the rectangle around the position of the owner
		r.x = static_cast<int>(start - mParallax*mOwner->GetGame()->GetCameraPos().x);
		r.y = static_cast<int>(0);
		
		// Draw (have to convert angle from radians to degrees, and clockwise to counter)
		SDL_RenderCopyEx(renderer,
			mTexture,
			nullptr,
			&r,
			0.0f,
			nullptr,
			SDL_FLIP_NONE);
		start += mTexWidth * mOwner->GetScale();
		
	}
	
	
	
}


