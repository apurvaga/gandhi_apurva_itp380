#include "PlayerMove.h"
#include "Game.h"
#include "SDL/SDL_mixer.h"
#include <algorithm>




PlayerMove::PlayerMove(Actor* actor)
	: MoveComponent(actor)
{
}


PlayerMove::~PlayerMove()
{
}

void PlayerMove::Update(float deltaTime)
{
	if (accelerateJump)
		mYSpeed -= JUMP_ACCELERATION * deltaTime;

	mOwner->SetPosition(mOwner->GetPosition() + mOwner->GetForward()*this->GetForwardSpeed()*deltaTime);
	if (mOwner->GetPosition().y + mYSpeed * deltaTime < mOwner->GetGame()->WINDOW_H)
		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y + mYSpeed * deltaTime));
	else {
		mInAir = false;
		mYSpeed = 0;
	}

	handleFall(deltaTime);

	//gravity
	if (mOwner->GetPosition().y + mYSpeed * deltaTime < mOwner->GetGame()->WINDOW_H)
		mYSpeed += mOwner->GetGame()->GRAVITY*deltaTime;

	mOwner->GetGame()->SetCameraPos(Vector2(mOwner->GetPosition().x - mOwner->GetGame()->WINDOW_W / 2, 0));
	if (int(mOwner->GetGame()->GetCameraPos().x + 1) % (mOwner->GetGame()->LEVEL_WIDTH) 
		>= (mOwner->GetGame()->LEVEL_WIDTH - mOwner->GetGame()->WINDOW_W)  && !loadedNextLevel) {
		mOwner->GetGame()->LoadNextLevel(); 
		loadedNextLevel = true;
	}
	else if(int(mOwner->GetGame()->GetCameraPos().x) % (mOwner->GetGame()->LEVEL_WIDTH) < (mOwner->GetGame()->LEVEL_WIDTH - mOwner->GetGame()->WINDOW_W)){
		loadedNextLevel = false; 
	}

	
}

void PlayerMove::ProcessInput(const Uint8 * state)
{
	/*if (state[SDL_SCANCODE_RIGHT]) {
		this->SetForwardSpeed(FORWARD_SPEED); 
	}
	else if (state[SDL_SCANCODE_LEFT]) {
		this->SetForwardSpeed(-FORWARD_SPEED); 
	}
	else {
		this->SetForwardSpeed(0); 
	}*/
	this->SetForwardSpeed(FORWARD_SPEED);

	//jump
	if (!mSpacePressedSpace && state[SDL_SCANCODE_SPACE] && !mInAir) {
		mYSpeed = -JUMP_SPEED; 
		mInAir = true; 

		// play sample on first free unreserved channel
		// play it exactly once through
		// Mix_Chunk *sample; //previously loaded
		if (Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Player/Jump.wav") , 0) == -1) {
			printf("Mix_PlayChannel: %s\n", Mix_GetError());
			// may be critical error, or maybe just no channels were free.
			// you could allocated another channel in that case...
		}

		jumpStartTime = SDL_GetTicks() / 1000.0f; 
	}
	else if (state[SDL_SCANCODE_SPACE] && mInAir) {
		if (SDL_GetTicks() / 1000.0f - jumpStartTime <= 0.3)
			accelerateJump = true;
		else
			accelerateJump = false; 
	}
	else {
		accelerateJump = false; 
	}

	mSpacePressedSpace = state[SDL_SCANCODE_SPACE];

}

void PlayerMove::handleFall(float deltaTime) {
	std::vector<Block*> blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {

			Vector2 blockMax = block->GetCollisionComponent()->GetMax();
			Vector2 blockMin = block->GetCollisionComponent()->GetMin();
			Vector2 playerMax = mOwner->GetCollisionComponent()->GetMax();
			Vector2 playerMin = mOwner->GetCollisionComponent()->GetMin();

			float dUp = Math::Abs(playerMax.y - blockMin.y);
			float dDown = Math::Abs(playerMin.y - blockMax.y);
			float dRight = Math::Abs(playerMin.x - blockMax.x);
			float dLeft = Math::Abs(playerMax.x - blockMin.x);

			float min = std::min(dUp, std::min(dDown, std::min(dRight, dLeft)));

			Vector2 position = mOwner->GetPosition();

			//right
			if (min == dRight) {
				mOwner->SetPosition(position + Vector2(dRight, 0));
			}

			//left
			else if (min == dLeft) {
				mOwner->SetPosition(position + Vector2(-dLeft, 0));
			}

			//down
			else if (min == dDown) {
				mOwner->SetPosition(position + Vector2(0, dDown));
				mYSpeed = 0.0f;
			}

			//up
			else {
				mOwner->SetPosition(position + Vector2(0, -dUp));
				mYSpeed = 0.0f;
				mInAir = false;
			}

		}
	}
}


