#include "Block.h"
#include "Game.h"
#include "Renderer.h"
#include "Mesh.h"



Block::Block(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this); 
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Cube.gpmesh"));
	mScale = 64.0f; 
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(1.0f, 1.0f, 1.0f);
	game->AddBlock(this);
}


Block::~Block()
{
	mGame->RemoveBlock(this);
}
