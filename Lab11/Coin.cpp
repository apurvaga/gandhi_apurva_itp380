#include "Coin.h"
#include "Game.h"
#include "Renderer.h"
#include "Mesh.h"
#include "Player.h"



Coin::Coin(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Coin.gpmesh"));
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(100.0f, 100.0f, 100.0f);
}


Coin::~Coin()
{
}

void Coin::UpdateActor(float deltaTime)
{
	mRotation += M_PI * deltaTime; 
	if (mCollide->Intersect(mGame->GetPlayer()->GetCollisionComponent())) {
		Mix_PlayChannel(-1, mGame->GetSound("Assets/Sounds/Coin.wav"), 0);
		SetState(EDead);
	}

}
