#include "Player.h"
#include "PlayerMove.h"
#include "CameraComponent.h"


Player::Player(Game* game)
	: Actor(game)
{
	mMove = new PlayerMove(this);
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(50.0f, 175.0f, 50.0f); 
	mCamera = new CameraComponent(this);
}


Player::~Player()
{
}
