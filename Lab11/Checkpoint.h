#pragma once
#include "Actor.h"
class Checkpoint :
	public Actor
{
public:
	Checkpoint(Game* game);
	~Checkpoint();
	void Activate() { active = true;  }
	void Deactivate() { active = false;  }
	void UpdateActor(float deltaTime) override;
	bool IsActive() { return active;  }
	void SetLevelString(std::string level) { mLevelString = level; };

private: 
	bool active = false; 
	std::string mLevelString; 
};

