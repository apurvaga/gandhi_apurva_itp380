#pragma once
#include "Component.h"
#include "Math.h"
class CameraComponent :
	public Component
{
public:
	CameraComponent(Actor* actor);
	~CameraComponent();
	void Update(float deltaTime) override; 
	float GetPitchSpeed() { return mPitchSpeed; }
	void SetPitchSpeed(float pitchSpeed) { mPitchSpeed = pitchSpeed; }

private: 
	float mPitchAngle = 0.0f; 
	float mPitchSpeed = 0.0f; 
};

