#pragma once
#include "Actor.h"
class Coin :
	public Actor
{
public:
	Coin(Game* game);
	~Coin();
	void UpdateActor(float deltaTime) override; 
};

