#include "Checkpoint.h"
#include "Game.h"
#include "Renderer.h"
#include "Mesh.h"
#include <queue>
#include "Player.h"


Checkpoint::Checkpoint(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Checkpoint.gpmesh"));
	mCollide = new CollisionComponent(this);
	mCollide->SetSize(25.0f, 25.0f, 25.0f);
}

void Checkpoint::UpdateActor(float deltaTime) {


	if (active)
		mMesh->SetTextureIndex(0);
	else
		mMesh->SetTextureIndex(1);
	
	
	
	if (active && GetCollisionComponent()->Intersect(mGame->GetPlayer()->GetCollisionComponent())) {
		
		mGame->GetCheckpoints().pop();
		if(!mGame->GetCheckpoints().empty())
			mGame->GetCheckpoints().front()->Activate();
		SetState(EDead);
		Mix_PlayChannel(-1, mGame->GetSound("Assets/Sounds/Checkpoint.wav"), 0);
		mGame->GetPlayer()->SetRespawnPos(mPosition);
		if (mLevelString.size() > 0 ) {
			mGame->SetNextLevel(mLevelString);
		}
	}
}


Checkpoint::~Checkpoint()
{
}
