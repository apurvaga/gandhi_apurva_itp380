#include "Arrow.h"
#include "Game.h"
#include "Renderer.h"
#include "Mesh.h"
#include "Player.h"
#include "Math.h"
#include "Checkpoint.h"

Arrow::Arrow(Game* game)
	: Actor(game)
{
	mMesh = new MeshComponent(this);
	mMesh->SetMesh(game->GetRenderer()->GetMesh("Assets/Arrow.gpmesh"));
	mScale = 0.15f;
}


Arrow::~Arrow()
{
}

void Arrow::UpdateActor(float deltaTime)
{
	if (mGame->GetActiveCheckpoint()) {
		Vector3 playerPos = mGame->GetPlayer()->GetPosition();
		Vector3 checkpointPos = mGame->GetActiveCheckpoint()->GetPosition();
		Vector3 displacementNormalized = checkpointPos - playerPos;
		displacementNormalized *= 1 / displacementNormalized.Length();

		float theta = acos(Vector3::Dot(displacementNormalized, Vector3(1, 0, 0)));
		Vector3 axis = Vector3::Cross(Vector3(1, 0, 0), displacementNormalized);
		axis *= 1 / axis.Length();
		if (Math::NearZero(axis.Length())) {
			mQuat = Quaternion::Identity; //identity
		}
		else {
			mQuat = Quaternion(axis, theta);
		}
	}
	else {
		mQuat = Quaternion::Identity; 
	}

	auto arrowPos = mGame->GetRenderer()->Unproject(Vector3(0.0f, 250.0f, 0.1f));
	this->SetPosition(arrowPos);

}
