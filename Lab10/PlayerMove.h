#pragma once
#include "MoveComponent.h"
#include <SDL/SDL.h>
#include "Math.h"
class PlayerMove :
	public MoveComponent
{
public:
	enum MoveState {
		OnGround, Jump, Falling, WallClimb, WallRun
	};
	enum CollSide {
		None, Top, Bottom, SideX1, SideX2, SideY1, SideY2
	};
	PlayerMove(Actor* actor);
	~PlayerMove();
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8 * keyState) override; 
	void ChangeState(MoveState state); 

protected: 
	void UpdateOnGround(float deltaTime);
	void UpdateJump(float deltaTime);
	void UpdateFalling(float deltaTime); 
	void UpdateWallClimb(float deltaTime);
	void UpdateWallRun(float deltaTime); 
	CollSide FixCollision(class CollisionComponent* self, class CollisionComponent* block);
	void PhysicsUpdate(float deltaTime); 
	void AddForce(const Vector3& force);
	void FixXYVelocity(); 
	bool CanWallClimb(CollSide side); 
	bool CanWallRun(CollSide side);
	MoveState mCurrentState; 
	const unsigned char jumpKey = SDL_SCANCODE_SPACE; 
	bool jumpKeyPressed = false; 
	Vector3 mVelocity;
	Vector3 mAcceleration;
	Vector3 mPendingForces; 
	float mMass = 1.0f; 
	Vector3 mGravity = Vector3(0.0f, 0.0f, -980.0f); 
	Vector3 mJumpForce = Vector3(0.0f, 0.0f, 35000.0f);
	const float maxSpeed = 400.0f; 
	const Vector3 mWallClimbForce = Vector3(0.0f, 0.0f, 1800.0f);
	float mWallClimbTimer = 0.0f; 
	const Vector3 mWallRunForce = Vector3(0.0f, 0.0f, 1200.0f); 
	float mWallRunTimer = 0.0f; 

};

