#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include "CameraComponent.h"
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector3::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
	,mMove(nullptr)
	,mCollide(nullptr)
	,mMesh(nullptr)
	,mCamera(nullptr)
{
	mGame->AddActor(this);
}

Actor::~Actor()
{
	mGame->RemoveActor(this);
	if(mMove)
		delete mMove; 
	if (mCollide)
		delete mCollide; 
	if (mMesh)
		delete mMesh; 
	if (mCamera)
		delete mCamera; 
}

void Actor::Update(float deltaTime)
{
	if (mState == EActive) {
		//update components
		if (mMove)
			mMove->Update(deltaTime);
		if (mCollide)
			mCollide->Update(deltaTime); 
		if (mMesh)
			mMesh->Update(deltaTime);
		if (mCamera)
			mCamera->Update(deltaTime);

		UpdateActor(deltaTime); 
		mWorldTransform = Matrix4::CreateScale(mScale) * Matrix4::CreateRotationZ(mRotation) * Matrix4::CreateTranslation(mPosition); 
	}
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const Uint8* keyState)
{
	if (mState == EActive) {
		//process input for components
		if (mMove)
			mMove->ProcessInput(keyState);
		if (mCollide)
			mCollide->ProcessInput(keyState); 
		if (mMesh)
			mMesh->ProcessInput(keyState);
		if (mCamera)
			mCamera->ProcessInput(keyState);
		

		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

Vector3 Actor::GetForward() const {
	return Vector3(cos(mRotation), sin(mRotation), 0.0f); 
}

Vector3 Actor::GetRight() const {
	return Vector3(cos(mRotation + Math::PiOver2), sin(mRotation + Math::PiOver2), 0.0f);
}
