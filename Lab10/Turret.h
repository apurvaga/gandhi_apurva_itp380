#pragma once
#include "Actor.h"
class Turret :
	public Actor
{
public:
	Turret(Game* game);
	void ActorInput(const Uint8* keyState) override;
	void SetPlayerTwo(); 
	~Turret();
protected: 
	unsigned char mCounterClockwise; 
	unsigned char mClockwise; 

};

