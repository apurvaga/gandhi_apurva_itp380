//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
using namespace std; 

int WIDTH = 20; 
int PADDLE_HEIGHT = 100; 
int BALL_DIM = 10; 
int PADDLE_SPEED = 400; 

// TODO
Game::Game() {
	initializeBall(); 
	initializePaddle(); 
	 
}

bool Game::initialize() {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return 0;
	}


	// Create an application window with the following settings:
	window = SDL_CreateWindow(
		"Pong",                  // window title
		SDL_WINDOWPOS_UNDEFINED,           // initial x position
		SDL_WINDOWPOS_UNDEFINED,           // initial y position
		1024,                               // width, in pixels
		768,                               // height, in pixels
		SDL_WINDOW_OPENGL                  // flags - see below
	);

	// Check that the window was successfully created
	if (window == NULL) {
		// In the case that the window could not be made...
		SDL_Log("Could not create window: %s\n", SDL_GetError());
		return 0;
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if((renderer) == NULL) {
		SDL_Log("Unable to initialize SDL renderer: %s", SDL_GetError());
		return 0;
	}

	currentTime = float(SDL_GetTicks()/1000); 
	return 1;
}

void Game::runLoop() {
	while (!quit) {
		processInput(); 
		updateGame();
		generateOutput(); 
	}
}

void Game::processInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		/* handle event here */
		switch (event.type) {
			case SDL_QUIT:
				quit = 1;
			}
	}
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE]) {
		quit = 1; 
	}
	else if (state[SDL_SCANCODE_UP]) {
		direction = -1; 
	}
	else if (state[SDL_SCANCODE_DOWN]) {
		direction = 1; 
	}
}

void Game::updateGame()
{
	float deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime; 
	

	//limit frames to 60 Hz
	while (deltaTime * 1000 < 16) {
		deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime;
	}
	currentTime = deltaTime + currentTime;

	//cout << deltaTime << endl; //check to see if game runs at 60 Hz
	
	paddlePosition.y = direction * PADDLE_SPEED * deltaTime + paddlePosition.y; 
	direction = 0; 
	if (paddlePosition.y > 768 - PADDLE_HEIGHT - WIDTH) {
		paddlePosition.y = 768 - PADDLE_HEIGHT - WIDTH;
	}
	else if (paddlePosition.y < WIDTH) {
		paddlePosition.y = WIDTH;
	}

	ballPosition.y += ballVelocity.y*deltaTime;
	ballPosition.x += ballVelocity.x*deltaTime; 
	handleCollision(); 
	hasLost(); 
}

void Game::generateOutput()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
	//clear
	SDL_RenderClear(renderer);

	//draw game objects

	//walls
	SDL_Rect rectTop;
	SDL_Rect rectRight;
	SDL_Rect rectBottom;
	//top wall
	rectTop.x = 0; 
	rectTop.y = 0; 
	rectTop.w = 1024; 
	rectTop.h = WIDTH; 

	//bottom wall
	rectBottom.x = 0;
	rectBottom.y = 768 - WIDTH;
	rectBottom.w = 1024;
	rectBottom.h = WIDTH;

	//right wall 
	rectRight.x = 1024 - WIDTH; 
	rectRight.y = 0; 
	rectRight.w = WIDTH; 
	rectRight.y = 768; 
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
	SDL_RenderFillRect(renderer, &rectTop);
	SDL_RenderFillRect(renderer, &rectRight);
	SDL_RenderFillRect(renderer, &rectBottom);

	//paddle
	SDL_Rect paddle; 
	paddle.x = paddlePosition.x; 
	paddle.y = paddlePosition.y; 
	paddle.h = PADDLE_HEIGHT; 
	paddle.w = WIDTH/2; 
	SDL_RenderFillRect(renderer, &paddle);

	//ball
	SDL_Rect ball; 
	ball.x = ballPosition.x; 
	ball.y = ballPosition.y; 
	ball.w = BALL_DIM; 
	ball.h = BALL_DIM;
	SDL_RenderFillRect(renderer, &ball); 

	//present
	SDL_RenderPresent(renderer);
}

void Game::shutdown() {
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}

void Game::initializePaddle() {
	paddlePosition.x = 10;
	paddlePosition.y = (768 / 2) - PADDLE_HEIGHT / 2;
}

void Game::initializeBall() {
	ballPosition.x = (1024 / 2) - BALL_DIM/2; 
	ballPosition.y = (768 / 2) - BALL_DIM/2; 
	//ballVelocity.x = -400; 
	//ballVelocity.y = 200; 
	//ballVelocity.x = -200; 
	//ballVelocity.y = 75; 
	ballVelocity.x = -300; 
	ballVelocity.y = 100; 
}

void Game::handleCollision() {
	if (ballPosition.y <= WIDTH) { //top wall
		ballPosition.y = WIDTH; 
		ballVelocity.y *= -1;
		//ballVelocity.y *= -1.1; 
	}

	if (ballPosition.x >= 1024 - BALL_DIM) { //right wall
		ballPosition.x = 1024 - BALL_DIM; 
		ballVelocity.x *= -1;
		//ballVelocity.x *= -1.1;
	}

	if (ballPosition.y >= 768 - WIDTH - BALL_DIM) { //bottom wall
		ballPosition.y = 768 - WIDTH - BALL_DIM; 
		ballVelocity.y *= -1; 
		//ballVelocity.y *= -1.1;
	}

	if (ballPosition.x <= 10 + WIDTH / 2 && ballPosition.x >= 10) {
		if (ballPosition.y >= paddlePosition.y && ballPosition.y <= paddlePosition.y + PADDLE_HEIGHT) {
			ballPosition.x = 10 + WIDTH / 2; 
			ballVelocity.x *= -1; 
			//ballVelocity.x *= -1.1;
		}

	}
}

void Game::hasLost() {
	if (ballPosition.x < 0)
		quit = 1; 
}