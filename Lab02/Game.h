#pragma once
#include "SDL/SDL.h"
#include <iostream>
// TODO
class Game {
	public:
		//constructor
		Game();

		bool initialize(); 

		void shutdown(); 

		void runLoop(); 


	private:
		SDL_Window * window;
		SDL_Renderer *renderer;
		SDL_Point paddlePosition; 
		SDL_Point ballPosition; 
		SDL_Point ballVelocity; 
		float currentTime; 
		char direction = 0; 
		bool quit = false; 
		void processInput(); 
		void updateGame(); 
		void generateOutput(); 
		void initializePaddle(); 
		void initializeBall();
		void handleCollision();

		void hasLost();


		
};