#include "BarrelSpawner.h"
#include "Barrel.h"



BarrelSpawner::BarrelSpawner(Game* game, Vector2 location) 
	: Actor(game), mLocation(location)
{
}


BarrelSpawner::~BarrelSpawner()
{
}

void BarrelSpawner::UpdateActor(float deltaTime)
{
	timer -= deltaTime; 
	if (timer <= 0.0f) {
		timer = BARREL_SPAWN_DELAY;
		Barrel* barrel = new Barrel(GetGame());
		barrel->SetPosition(mLocation);
	}
}
