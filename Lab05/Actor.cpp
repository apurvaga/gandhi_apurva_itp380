#include "Actor.h"
#include "Game.h"
#include "Component.h"
#include <algorithm>

Actor::Actor(Game* game)
	:mGame(game)
	,mState(EActive)
	,mPosition(Vector2::Zero)
	,mScale(1.0f)
	,mRotation(0.0f)
	,mSprite(nullptr)
	,mMove(nullptr)
	,mCollide(nullptr)
{
	mGame->AddActor(this);
}

Actor::~Actor()
{
	mGame->RemoveActor(this);
	if(mSprite)
		delete mSprite;
	if(mMove)
		delete mMove; 
	if (mCollide)
		delete mCollide; 
}

void Actor::Update(float deltaTime)
{
	if (mState == EActive) {
		//update components
		if (mMove)
			mMove->Update(deltaTime);
		if (mSprite)
			mSprite->Update(deltaTime);
		if (mCollide)
			mCollide->Update(deltaTime); 

		UpdateActor(deltaTime); 
	}
}

void Actor::UpdateActor(float deltaTime)
{
}

void Actor::ProcessInput(const Uint8* keyState)
{
	if (mState == EActive) {
		//process input for components
		if (mMove)
			mMove->ProcessInput(keyState);
		if(mSprite)
			mSprite->ProcessInput(keyState); 
		if (mCollide)
			mCollide->ProcessInput(keyState);
		

		ActorInput(keyState);
	}
}

void Actor::ActorInput(const Uint8* keyState)
{
}

Vector2 Actor::GetForward() {
	return Vector2(cos(mRotation), -1 *sin(mRotation)); 
}
