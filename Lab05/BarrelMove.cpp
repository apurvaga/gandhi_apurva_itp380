#include "BarrelMove.h"
#include "Barrel.h"
#include "Actor.h"



BarrelMove::BarrelMove(Actor* actor)
	: MoveComponent(actor)
{
	SetForwardSpeed(100.0f); 
	SetAngularSpeed(-2*M_PI); 
}

void BarrelMove::Update(float deltaTime)
{
	if (mOwner->GetPosition().y > mOwner->GetGame()->WINDOW_H)
		mOwner->SetState(mOwner->EDead); 

	mOwner->SetRotation(mOwner->GetRotation() + deltaTime*GetAngularSpeed()); 
	mOwner->SetPosition(mOwner->GetPosition() + Vector2(1,0)*this->GetForwardSpeed()*deltaTime);
	mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y + mYSpeed * deltaTime));
	//gravity
	mYSpeed += mOwner->GetGame()->GRAVITY*deltaTime;

	//handle collison with blocks
	std::vector<Block*> blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {
			mYSpeed = 0.0f; 
			mOwner->SetPosition(mOwner->GetPosition() + Vector2(0,  block->GetCollisionComponent()->GetMin().y - mOwner->GetCollisionComponent()->GetMax().y));
		}
	}

	//handle collision with player
	Actor* player = mOwner->GetGame()->GetPlayer(); 
	if (player->GetCollisionComponent()->Intersect(mOwner->GetCollisionComponent())) {
		player->SetPosition(mOwner->GetGame()->GetInitialPlayerPosition()); 
	}
}


BarrelMove::~BarrelMove()
{
}
