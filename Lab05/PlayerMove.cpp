#include "PlayerMove.h"
#include "Game.h"
#include "SDL/SDL_mixer.h"
#include <algorithm>



PlayerMove::PlayerMove(Actor* actor)
	: MoveComponent(actor)
{
}


PlayerMove::~PlayerMove()
{
}

void PlayerMove::Update(float deltaTime)
{
	mOwner->SetPosition(mOwner->GetPosition() + mOwner->GetForward()*this->GetForwardSpeed()*deltaTime); 
	if (mOwner->GetPosition().y + mYSpeed * deltaTime < mOwner->GetGame()->WINDOW_H)
		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetPosition().y + mYSpeed * deltaTime));
	else {
		mInAir = false;
		mYSpeed = 0; 
	}
	
	handleFall(deltaTime);

	//gravity
	if (mOwner->GetPosition().y  + mYSpeed * deltaTime < mOwner->GetGame()->WINDOW_H)
		mYSpeed += mOwner->GetGame()->GRAVITY*deltaTime;
	
	 
	
}

void PlayerMove::ProcessInput(const Uint8 * state)
{
	if (state[SDL_SCANCODE_RIGHT]) {
		this->SetForwardSpeed(FORWARD_SPEED); 
	}
	else if (state[SDL_SCANCODE_LEFT]) {
		this->SetForwardSpeed(-FORWARD_SPEED); 
	}
	else {
		this->SetForwardSpeed(0); 
	}

	//jump
	if (!mSpacePressedSpace && state[SDL_SCANCODE_SPACE] && !mInAir) {
		mYSpeed = -JUMP_SPEED; 
		mInAir = true; 

		// play sample on first free unreserved channel
		// play it exactly once through
		// Mix_Chunk *sample; //previously loaded
		if (Mix_PlayChannel(-1, mOwner->GetGame()->GetSound("Assets/Player/Jump.wav") , 0) == -1) {
			printf("Mix_PlayChannel: %s\n", Mix_GetError());
			// may be critical error, or maybe just no channels were free.
			// you could allocated another channel in that case...
		}
	}

	mSpacePressedSpace = state[SDL_SCANCODE_SPACE];

}

void PlayerMove::handleFall(float deltaTime) {
	std::vector<Block*> blocks = mOwner->GetGame()->GetBlocks();
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {

			Vector2 blockMax = block->GetCollisionComponent()->GetMax();
			Vector2 blockMin = block->GetCollisionComponent()->GetMin();
			Vector2 playerMax = mOwner->GetCollisionComponent()->GetMax();
			Vector2 playerMin = mOwner->GetCollisionComponent()->GetMin();

			float dUp = Math::Abs(playerMax.y - blockMin.y);
			float dDown = Math::Abs(playerMin.y - blockMax.y);
			float dRight = Math::Abs(playerMin.x - blockMax.x);
			float dLeft = Math::Abs(playerMax.x - blockMin.x);

			float min = std::min(dUp, std::min(dDown, std::min(dRight, dLeft)));

			Vector2 position = mOwner->GetPosition();

			//right
			if (min == dRight) {
				mOwner->SetPosition(position + Vector2(dRight, 0));
			}

			//left
			else if (min == dLeft) {
				mOwner->SetPosition(position + Vector2(-dLeft, 0));
			}

			//down
			else if (min == dDown) {
				mOwner->SetPosition(position + Vector2(0, dDown));
				mYSpeed = 0.0f;
			}

			//up
			else {
				mOwner->SetPosition(position + Vector2(0, -dUp));
				mYSpeed = 0.0f;
				mInAir = false;
			}

		}
	}
}

//arctan2 method of handling collision
/*void PlayerMove::handleFall(float deltaTime) {
	std::vector<Block*> blocks = mOwner->GetGame()->GetBlocks(); 
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {
			Vector2 displacement = mOwner->GetPosition() - block->GetPosition();
			float theta = atan2(displacement.y, displacement.x);
			float magicNumber = 0;
			//right
			if (theta < M_PI / 6 - magicNumber && theta > -M_PI / 6 + magicNumber) {
				mOwner->SetPosition(Vector2(block->GetCollisionComponent()->GetMax().x + mOwner->GetCollisionComponent()->GetWidth() / 2, mOwner->GetPosition().y));

			}

			//left
			else if (theta < -5 * M_PI / 6 - magicNumber && theta >= -M_PI + magicNumber || theta >= 5 * M_PI / 6 + magicNumber && theta <= M_PI - magicNumber) {
				mOwner->SetPosition(Vector2(block->GetCollisionComponent()->GetMin().x - mOwner->GetCollisionComponent()->GetWidth() / 2, mOwner->GetPosition().y));
			}

			//down
			//else if (mOwner->GetPosition().y > block->GetPosition().y) {
			else if(theta <= 5*M_PI/6  + magicNumber && theta >= M_PI/6 - magicNumber){
			//else if (velocity.y < 0) {
			//else if(mYSpeed < 0) {
				mOwner->SetPosition(Vector2(mOwner->GetPosition().x, block->GetCollisionComponent()->GetMax().y + mOwner->GetCollisionComponent()->GetHeight()/2 ));
				mYSpeed = 0.0f; 
			}

			//up
			else {
				mOwner->SetPosition(Vector2(mOwner->GetPosition().x, block->GetCollisionComponent()->GetMin().y - mOwner->GetCollisionComponent()->GetHeight() / 2));
				mYSpeed = 0.0f; 
				mInAir = false; 
			}

		}
	}
}*/



