#pragma once
#include "MoveComponent.h"
class PlayerMove :
	public MoveComponent
{
public:
	PlayerMove(Actor* actor);
	virtual ~PlayerMove();
	void Update(float deltaTime) override;
	void ProcessInput(const Uint8* keyState) override;

	void handleFall(float deltaTime);

protected: 
	float mYSpeed = 0.0f; 
	bool mSpacePressedSpace = false; 
	bool mInAir = false; 
	const float JUMP_SPEED = 800.0f; 
	const float FORWARD_SPEED = 300.0f; 
};

