#pragma once
#include "MoveComponent.h"

class BarrelMove :
	public MoveComponent
{
public:
	BarrelMove(Actor* actor);
	void Update(float deltaTime) override; 
	~BarrelMove();

protected: 
	float mYSpeed = 0.0f; 
};

