#include "Player.h"
#include "Game.h"
#include "PlayerMove.h"


Player::Player(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this, 200); 
	mSprite->SetTexture(mGame->getTexture("Assets/Player/Idle.png")); 
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(20, 64);
	mMove = new PlayerMove(this); 
}


Player::~Player()
{
}
