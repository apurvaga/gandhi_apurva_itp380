#include "BallMove.h"
#include "Actor.h"
#include "Game.h"
#include "Paddle.h"
#include "CollisionComponent.h"



BallMove::BallMove(Actor* actor)
	: MoveComponent(actor)
{
	velocity = Vector2(250, -250); 
}

void BallMove::Update(float deltaTime)
{
	mOwner->SetPosition(mOwner->GetPosition() + (velocity * deltaTime));

	//left wall collision
	if (mOwner->GetPosition().x < 32 + mOwner->GetSprite()->GetTexWidth() / 2) {
		mOwner->SetPosition(Vector2(32 + mOwner->GetSprite()->GetTexWidth() / 2, mOwner->GetPosition().y));
		velocity.x *= -1;
	}

	//right wall collision
	else if (mOwner->GetPosition().x > 1024 - 32 - +mOwner->GetSprite()->GetTexWidth() / 2) {
		mOwner->SetPosition(Vector2(1024 - 32 - +mOwner->GetSprite()->GetTexWidth() / 2, mOwner->GetPosition().y));
		velocity.x *= -1;
	}

	//top wall collision
	else if (mOwner->GetPosition().y < 32 + mOwner->GetSprite()->GetTexHeight() / 2) {
		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, 32 + mOwner->GetSprite()->GetTexHeight() / 2));
		velocity.y *= -1;
	}

	//goes off screen at the bottom
	else if (mOwner->GetPosition().y > 1024) {
		mOwner->SetPosition(Vector2(1024 / 2, mOwner->GetGame()->PADDLE_Y - 30));
		velocity = Vector2(250, -250);
	}

	//check and handle paddle collision
	if (mOwner->GetCollisionComponent()->Intersect(mOwner->GetGame()->getPaddle()->GetCollisionComponent())) {

		mOwner->SetPosition(Vector2(mOwner->GetPosition().x, mOwner->GetGame()->getPaddle()->GetCollisionComponent()->GetMin().y - mOwner->GetCollisionComponent()->GetHeight()/2)); 

		Vector2 paddlePosition = mOwner->GetGame()->getPaddle()->GetCollisionComponent()->GetMin();
		Vector2 position = mOwner->GetPosition();
		int thirdWidth = mOwner->GetGame()->getPaddle()->GetCollisionComponent()->GetWidth() / 3;

		if (velocity.y >= 0) {
			//left third 
			if (position.x < paddlePosition.x + thirdWidth) {
				//reflect with normal tilted to the left by 15 degrees
				velocity = Vector2::Reflect(velocity, Vector2(cos(M_PI + 5*M_PI/12), sin(M_PI + 5 * M_PI / 12)));
			}

			//right third
			else if (position.x > paddlePosition.x + 2 * thirdWidth) {
				//reflect with normal tilted to the right by 15 degrees
				velocity = Vector2::Reflect(velocity, Vector2(cos(2 * M_PI - 5 * M_PI / 12), sin(2 * M_PI - 5 * M_PI / 12)));
			}
			else {
				velocity = Vector2::Reflect(velocity, Vector2(cos(3 * M_PI / 2), sin(3 * M_PI / 2)));
			}
		}
	}	

	//check and handle block collision
	std::vector<Block*> blocks = mOwner->GetGame()->getBlocks(); 
	for (auto block : blocks) {
		if (mOwner->GetCollisionComponent()->Intersect(block->GetCollisionComponent())) {
			block->SetState(block->EDead); 
			Vector2 displacement = mOwner->GetPosition() - block->GetPosition(); 
			float theta = atan2(displacement.y, displacement.x); 
			float magicNumber = 0;
			//right
			if (theta < M_PI / 6 - magicNumber && theta > -M_PI / 6 + magicNumber) {
				mOwner->SetPosition(Vector2(block->GetCollisionComponent()->GetMax().x + mOwner->GetCollisionComponent()->GetWidth() / 2, mOwner->GetPosition().y));
				velocity = Vector2::Reflect(velocity, Vector2(cos(0), sin(0)));
				
			}

			//left
			else if (theta < -5*M_PI/6 - magicNumber && theta >= -M_PI + magicNumber || theta >= 5*M_PI/6  + magicNumber && theta <= M_PI - magicNumber) {
				mOwner->SetPosition(Vector2(block->GetCollisionComponent()->GetMin().x - mOwner->GetCollisionComponent()->GetWidth() / 2, mOwner->GetPosition().y));
				velocity = Vector2::Reflect(velocity, Vector2(cos(M_PI), sin(M_PI)));
			}

			//down
			//else if (mOwner->GetPosition().y > block->GetPosition().y) {
			else if(theta <= 5*M_PI/6  + magicNumber && theta >= M_PI/6 - magicNumber){
			//else if(velocity.y < 0){
				mOwner->SetPosition(Vector2( mOwner->GetPosition().x, block->GetCollisionComponent()->GetMax().y + mOwner->GetCollisionComponent()->GetHeight() / 2));
				velocity = Vector2::Reflect(velocity, Vector2(cos(M_PI / 2), sin(M_PI / 2)));

			}

			//up
			else {
				mOwner->SetPosition(Vector2(mOwner->GetPosition().x, block->GetCollisionComponent()->GetMin().y - mOwner->GetCollisionComponent()->GetHeight() / 2));
				velocity = Vector2::Reflect(velocity, Vector2(cos(-M_PI / 2), sin(-M_PI / 2)));

			}
			return; 

		}
	}
}


BallMove::~BallMove()
{
}
