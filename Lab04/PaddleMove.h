#pragma once
#include "MoveComponent.h"
class PaddleMove :
	public MoveComponent
{
public:
	PaddleMove(Actor* actor);
	~PaddleMove();
	void Update(float deltaTime) override;
};

