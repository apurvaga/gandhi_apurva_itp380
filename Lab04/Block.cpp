#include "Block.h"
#include "Game.h"
#include <string>



void Block::changeTexture(std::string blockType)
{
	mSprite->SetTexture(mGame->getTexture("Assets/Block" + blockType + ".png")); 
}

Block::Block(Game* game, std::string blockType)
	: Actor(game)
{
	mSprite = new SpriteComponent(this);
	changeTexture(blockType); 
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(64, 32); 
	mGame->AddBlock(this);
}


Block::~Block()
{
	mGame->RemoveBlock(this); 
}
