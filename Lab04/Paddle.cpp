#include "Paddle.h"
#include "Game.h"
#include "PaddleMove.h"



Paddle::Paddle(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this);
	mSprite->SetTexture(mGame->getTexture("Assets/Paddle.png"));
	PaddleMove* move = new PaddleMove(this);
	mMove = move;
	mMove->SetAngularSpeed(0);
	mMove->SetForwardSpeed(0);
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(104, 24); 
}


Paddle::~Paddle()
{
}

void Paddle::ActorInput(const Uint8 * keyState)
{
	if (keyState[SDL_SCANCODE_RIGHT]) {
		mMove->SetForwardSpeed(400);
	}
	else if (keyState[SDL_SCANCODE_LEFT]) {
		mMove->SetForwardSpeed(-400);
	}
	else
		mMove->SetForwardSpeed(0);
	
}