#pragma once
#include "Actor.h"
class Paddle :
	public Actor
{
public:
	Paddle(Game* game);
	~Paddle();
	void ActorInput(const Uint8 * keyState);
};

