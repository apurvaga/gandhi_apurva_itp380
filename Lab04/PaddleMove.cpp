#include "PaddleMove.h"
#include "Actor.h"


PaddleMove::PaddleMove(Actor* actor)
	: MoveComponent(actor)
{
}


PaddleMove::~PaddleMove()
{
}

void PaddleMove::Update(float deltaTime)
{
	MoveComponent::Update(deltaTime);
	if (mOwner->GetPosition().x < 32 + mOwner->GetSprite()->GetTexWidth() / 2)
		mOwner->SetPosition(Vector2(32 + mOwner->GetSprite()->GetTexWidth() / 2, mOwner->GetPosition().y)); 
	else if(mOwner->GetPosition().x > 1024 - 32 -  + mOwner->GetSprite()->GetTexWidth() / 2)
		mOwner->SetPosition(Vector2(1024 - 32 - +mOwner->GetSprite()->GetTexWidth() / 2, mOwner->GetPosition().y));
}
