#include "Ball.h"
#include "Game.h"
#include "BallMove.h"


Ball::Ball(Game* game)
	: Actor(game)
{
	mSprite = new SpriteComponent(this);
	mSprite->SetTexture(mGame->getTexture("Assets/Ball.png")); 
	mMove = new BallMove(this);
	mMove->SetAngularSpeed(0);
	mMove->SetForwardSpeed(0);
	mCollide = new CollisionComponent(this); 
	mCollide->SetSize(10, 10); 
}


Ball::~Ball()
{
}
