//
//  Game.cpp
//  Game-mac
//
//  Created by Sanjay Madhav on 5/31/17.
//  Copyright © 2017 Sanjay Madhav. All rights reserved.
//

#include "Game.h"
#include <SDL/SDL_image.h>
#include "Actor.h"
#include <algorithm>
#include "Block.h"
#include <fstream>
#include <sstream>
#include "paddle.h"
#include "ball.h"
using namespace std; 

 

// TODO
Game::Game() {

	 
}

bool Game::initialize() {
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return 0;
	}


	// Create an application window with the following settings:
	window = SDL_CreateWindow(
		"Asteroids",                  // window title
		SDL_WINDOWPOS_UNDEFINED,           // initial x position
		SDL_WINDOWPOS_UNDEFINED,           // initial y position
		1024,                               // width, in pixels
		768,                               // height, in pixels
		SDL_WINDOW_OPENGL                  // flags - see below
	);

	// Check that the window was successfully created
	if (window == NULL) {
		// In the case that the window could not be made...
		SDL_Log("Could not create window: %s\n", SDL_GetError());
		return 0;
	}
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if((renderer) == NULL) {
		SDL_Log("Unable to initialize SDL renderer: %s", SDL_GetError());
		return 0;
	}

	IMG_Init(IMG_INIT_PNG);
	loadData();
	currentTime = float(SDL_GetTicks()/1000); 
	return 1;
}

void Game::runLoop() {
	while (!quit) {
		processInput(); 
		updateGame();
		generateOutput(); 
	}
}

void Game::processInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event)) {
		/* handle event here */
		switch (event.type) {
			case SDL_QUIT:
				quit = 1;
			}
	}
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	if (state[SDL_SCANCODE_ESCAPE]) {
		quit = 1; 
	}
	else if (state[SDL_SCANCODE_UP]) {
		direction = -1; 
	}
	else if (state[SDL_SCANCODE_DOWN]) {
		direction = 1; 
	}
	for (Actor* actor : actors) {
		actor->ProcessInput(state);
	}

}

void Game::updateGame()
{
	float deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime; 
	

	//limit frames to 60 Hz
	while (deltaTime * 1000 < 16) {
		deltaTime = (float(SDL_GetTicks()) / 1000) - currentTime;
	}

	//put max limit on delta time for debugging purposes
	deltaTime = (deltaTime > 0.05) ? 0.05 : deltaTime; 

	currentTime = deltaTime + currentTime;

	//cout << deltaTime << endl; //check to see if game runs at 60 Hz

	auto tempActors = actors; 
	vector<Actor*> deadActors; 
	for (auto actor : tempActors) {
		actor->Update(deltaTime);
	}
	
	for (auto actor : actors) {
		if (actor->GetState() == actor->EDead) {
			deadActors.push_back(actor);
		}
	}

	for (auto actor : deadActors) {
		//RemoveActor(actor);
		delete actor; 
	}

	
}

void Game::generateOutput()
{
	SDL_SetRenderDrawColor(renderer, 0, 0, 255, 255);
	//clear
	SDL_RenderClear(renderer);

	//draw game objects
	for (auto sprite : sprites)
		sprite->Draw(renderer); 


	//present
	SDL_RenderPresent(renderer);
}

void Game::loadTexture(std::string fileName)
{
	SDL_Surface* surface = IMG_Load(fileName.c_str());
	SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
	SDL_FreeSurface(surface);
	textures[fileName] = texture; 
}

SDL_Texture * Game::getTexture(std::string name)
{
	return textures.find(name) != textures.end() ? textures[name] : nullptr;
}

void Game::readLevel(std::string filename)
{
	ifstream ifile;
	ifile.open(filename); 
	string positions[15][15]; 
	for (int i = 0; i < 225; i++) {
		char type; 
		ifile.get(type);
		if (type == 'A' || type == 'B' || type == 'C' || type == 'D' || type == 'E' || type == 'F') {
			Block* block = new Block(this, std::string(1, type));
			block->SetPosition(Vector2((i % 15) * 64 + 64, (i / 15) * 32 + 48));
		}
		else if (type != '.') {
			i--;
		}
	}
}

void Game::AddActor(Actor* actor) {
	actors.push_back(actor); 
}

void Game::RemoveActor(Actor* actor) {
	actors.erase(std::remove(actors.begin(), actors.end(), actor), actors.end());
}

void Game::AddSprite(SpriteComponent * sprite) {
	sprites.push_back(sprite);

	std::sort(sprites.begin(), sprites.end(),
		[](SpriteComponent* a, SpriteComponent* b) {
		return a->GetDrawOrder() < b->GetDrawOrder();
	});
}

void Game::AddBlock(Block * block)
{
	blocks.push_back(block); 
}

void Game::RemoveSprite(SpriteComponent * sprite)
{
	sprites.erase(std::remove(sprites.begin(), sprites.end(), sprite), sprites.end());
}

void Game::RemoveBlock(Block* block) {
	blocks.erase(std::remove(blocks.begin(), blocks.end(), block), blocks.end());
}

void Game::loadData() {
	loadTexture("Assets/Background.png");
	loadTexture("Assets/Ball.png");
	loadTexture("Assets/BlockA.png");
	loadTexture("Assets/BlockB.png");
	loadTexture("Assets/BlockC.png");
	loadTexture("Assets/BlockD.png");
	loadTexture("Assets/BlockE.png");
	loadTexture("Assets/BlockF.png");
	loadTexture("Assets/Paddle.png");

	Actor* background = new Actor(this);
	SpriteComponent* walls = new SpriteComponent(background, 50);
	walls->SetTexture(getTexture("Assets/Background.png"));
	background->SetSprite(walls);
	background->SetPosition(Vector2(1024/2, 768/2)); 

	readLevel("Assets/Level.txt"); 

	paddle = new Paddle(this);
	paddle->SetPosition(Vector2(1024/2, PADDLE_Y));
	Ball* ball = new Ball(this);
	ball->SetPosition(Vector2(1024/2, PADDLE_Y - 30)); 

	
}

void Game::unloadData() {
	while (!actors.empty()) {
		actors.pop_back(); 
	}
	for (auto texturePair : textures) {
		SDL_DestroyTexture(texturePair.second); 
	}
}

void Game::shutdown() {
	unloadData(); 
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	IMG_Quit(); 
	SDL_Quit();
}



