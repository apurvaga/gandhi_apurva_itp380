#pragma once
#include "MoveComponent.h"
#include "Math.h"
#include "Actor.h"
class BallMove :
	public MoveComponent
{
public:
	BallMove(Actor* actor);
	void Update(float deltaTime) override;
	~BallMove();

protected: 
	Vector2 velocity; 
};

