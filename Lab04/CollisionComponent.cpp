#include "CollisionComponent.h"
#include "Actor.h"

CollisionComponent::CollisionComponent(class Actor* owner)
:Component(owner)
,mWidth(0.0f)
,mHeight(0.0f)
{
	
}

CollisionComponent::~CollisionComponent()
{
	
}

bool CollisionComponent::Intersect(const CollisionComponent* other)
{
	Vector2 otherMax = other->GetMax(); 
	Vector2 otherMin = other->GetMin(); 
	Vector2 max = GetMax(); 
	Vector2 min = GetMin(); 

	if (max.x < otherMin.x || max.y < otherMin.y
		|| otherMax.x < min.x || otherMax.y < min.y)
		return false; 

	return true; 
}

Vector2 CollisionComponent::GetMin() const
{
	return Vector2(mOwner->GetPosition().x - (mWidth*mOwner->GetScale() / 2.0),
		mOwner->GetPosition().y - (mHeight*mOwner->GetScale() / 2.0)); 
}

Vector2 CollisionComponent::GetMax() const
{
	return Vector2(mOwner->GetPosition().x + (mWidth*mOwner->GetScale() / 2.0),
		mOwner->GetPosition().y + (mHeight*mOwner->GetScale() / 2.0));
}

const Vector2& CollisionComponent::GetCenter() const
{
	return mOwner->GetPosition();
}

