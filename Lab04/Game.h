#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include "Block.h"

class Actor; 
class Paddle; 
class SpriteComponent; 
// TODO
class Game {
	public:
		//constructor
		Game();

		bool initialize(); 

		void shutdown(); 

		void runLoop(); 

		void AddActor(Actor* actor);
		void RemoveActor(Actor* actor);
		void AddSprite(SpriteComponent* sprite);
		void AddBlock(Block* block);
		void RemoveSprite(SpriteComponent* sprite);
		void RemoveBlock(Block * block);
		void loadData();
		void unloadData(); 
		SDL_Texture* getTexture(std::string);

		void readLevel(std::string filename); 
		

		const int WIDTH = 20;
		const int PADDLE_HEIGHT = 100;
		const int BALL_DIM = 10;
		const int PADDLE_SPEED = 400;

		const int PADDLE_Y = 730;

		Paddle* getPaddle() { return paddle; }
		std::vector<Block*> getBlocks() { return blocks; }
		


	private:
		SDL_Window * window;
		SDL_Renderer *renderer;
		std::vector<Actor*> actors;
		std::vector<SpriteComponent*> sprites; 
		SDL_Texture* background; 
		std::unordered_map<std::string, SDL_Texture*> textures; 
		float currentTime; 
		char direction = 0; 
		bool quit = false; 
		void processInput(); 
		void updateGame(); 
		void generateOutput(); 
		void loadTexture(std::string); 
		Paddle* paddle; 
		std::vector<Block*> blocks; 
		
		

	



		
};