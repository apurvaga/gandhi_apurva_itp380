#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include <unordered_map>

class Actor; 
class SpriteComponent; 
// TODO
class Game {
	public:
		//constructor
		Game();

		bool initialize(); 

		void shutdown(); 

		void runLoop(); 

		void AddActor(Actor* actor);
		void RemoveActor(Actor* actor);
		void AddSprite(SpriteComponent* sprite);
		void RemoveSprite(SpriteComponent* sprite);
		void loadData();
		void unloadData(); 
		SDL_Texture* getTexture(std::string);


	private:
		SDL_Window * window;
		SDL_Renderer *renderer;
		std::vector<Actor*> actors;
		std::vector<SpriteComponent*> sprites; 
		SDL_Texture* background; 
		std::unordered_map<std::string, SDL_Texture*> textures; 
		float currentTime; 
		char direction = 0; 
		bool quit = false; 
		void processInput(); 
		void updateGame(); 
		void generateOutput(); 
		void loadTexture(std::string); 
		
		

	



		
};