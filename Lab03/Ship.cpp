#include "Ship.h"
#include "Game.h"


Ship::Ship(Game* game)
	:Actor(game)
{
	
	mSprite = new SpriteComponent(this);
	mSprite->SetTexture(mGame->getTexture("Assets/Ship.png"));
	MoveComponent* move = new MoveComponent(this);
	//move->SetAngularSpeed(0.30); 
	//move->SetForwardSpeed(200);
	mMove = move; 
}


Ship::~Ship()
{
	
}

void Ship::ActorInput(const Uint8 * keyState)
{
	if (keyState[SDL_SCANCODE_UP]) {
		 mMove->SetForwardSpeed(200);
		 mSprite->SetTexture(mGame->getTexture("Assets/ShipThrust.png"));
	}
	else if (keyState[SDL_SCANCODE_DOWN]) {
		mMove->SetForwardSpeed(-200);
		mSprite->SetTexture(mGame->getTexture("Assets/ShipThrust.png"));
	}
	else {
		mMove->SetForwardSpeed(0); 
		mSprite->SetTexture(mGame->getTexture("Assets/Ship.png"));
	}
	if (keyState[SDL_SCANCODE_RIGHT]) {
		mMove->SetAngularSpeed(-3.14); 
	}
	else if (keyState[SDL_SCANCODE_LEFT]) {
		mMove->SetAngularSpeed(3.14);
	}
	else {
		mMove->SetAngularSpeed(0); 
	}
}
