#pragma once
#include "Actor.h"
class Ship :
	public Actor
{
public:
	Ship(Game* game);
	~Ship();
	virtual void ActorInput(const Uint8* keyState) override;
};

